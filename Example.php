<?php namespace escholar\test\sdk;
require __DIR__ . '/vendor/autoload.php';

use Exception;

use escholar\sdk\{ApiClient,Token};
use escholar\sdk\Requests\{DataCollectionsRequest,DataFileGroupsRequest};
use escholar\sdk\RequestModel\DataFileGroup;
use escholar\sdk\Templates\Student;

// configuration vars
$user     = '';
$token    = '';
$apiRoot  = '';
$dataCollectionId = NULL;

// make a new token with our test credentials
$token = Token::withCredentials($user, $token);

// make a new client instance
$client = new ApiClient($apiRoot, $token);

// make a new collection request object
$r = new DataCollectionsRequest($client);

// get the list of collections
try {
    $collections = $r->listDataCollections();

    // display the collections
    foreach ($collections as $collection) {
      echo $collection->name . " (" . $collection->id . ")". PHP_EOL;

      foreach ($collection->templates as $template) {
        echo $template->number . " " . $template->name . PHP_EOL;
      }

      echo PHP_EOL;
    }
} catch (Exception $e) {
    echo "Error: " . $e->getMessage() . PHP_EOL;
}

// make a data file group request instance for data collection
$dataFileGroupRequest = new DataFileGroupsRequest($client, $dataCollectionId);

// create an array for student data
$data = array(
  "studentId" => '83794',
  "schoolYearDate" => '2017-06-30',
  "locationCode" => '100120013001',
  "districtCode" => '2251001',
  "firstNameShort" => 'John',
  "lastNameShort" => 'Smith',
);

// create a student
$student = new Student($data);

// create a new data file group to save
$dataFileGroup = new DataFileGroup();
$dataFileGroup->setBatchComments('Batch created from PHP SDK');
$dataFileGroup->setStudents(array($student));

$createdId = NULL;

// save the data file group
try {
  $dataFileGroupResponse =
    $dataFileGroupRequest->createDataFileGroup($dataFileGroup);

  // read the response to get the newly created id
  $createdId = $dataFileGroupResponse->id;
  echo "Created Data File Group ", $createdId, PHP_EOL;
} catch (Exception $e) {
  echo "Error: " . $e->getMessage() . PHP_EOL;
}

// verify we created a new data file group
if ($createdId == NULL) {
  exit;
}

// request data file group with id and read status
try {
  $status = $dataFileGroupRequest->fetchDataFileGroup($createdId)->status;
  echo "Data File Group: ", $createdId, ", has status: ", $status, PHP_EOL;

} catch (Exception $e) {
  echo "Error: " . $e->getMessage() . PHP_EOL;
}

?>
