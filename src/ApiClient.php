<?php namespace escholar\sdk;
use GuzzleHttp\Client;

class ApiClient {
  public $client;

  public function __construct($apiRoot, $token) {
      $this->client = new Client([
        'base_uri'  => $apiRoot,
        'headers'   => [
            'Authorization' => $token->headerString,
            'Content-Type'  => 'application/json'
        ]
      ]);
  }
}

?>
