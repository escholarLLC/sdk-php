<?php namespace escholar\sdk\RequestModel;
use JsonSerializable;

class DataFileGroup implements JsonSerializable {
  private $id;
  private $batchComments;
	private $courses;
	private $courseInstructorSnapshots;
	private $cteStudentIndustryCredentials;
	private $cteStudentFacts;
	private $districtSnapshots;
	private $programsFacts;
	private $pimsSchoolCalendars;
	private $schoolEnrollments;
	private $specialEducationSnapshots;
	private $staffs;
	private $staffAssignments;
	private $staffSnapshots;
	private $students;
	private $pimStudentCalendarFacts;
	private $studentCourseEnrollments;
	private $studentFacts;
	private $studentSnapshots;

  public function getId()
  {
     return $this->id;
  }

  public function setId($id)
  {
     $this->id = $id;
  }

  public function getBatchComments()
  {
     return $this->batchComments;
  }

  public function setBatchComments($batchComments)
  {
     $this->batchComments = $batchComments;
  }

  public function getCourses()
  {
     return $this->courses;
  }

  public function setCourses($courses)
  {
     $this->courses = $courses;
  }

  public function getCourseInstructorSnapshots()
  {
     return $this->courseInstructorSnapshots;
  }

  public function setCourseInstructorSnapshots($courseInstructorSnapshots)
  {
     $this->courseInstructorSnapshots = $courseInstructorSnapshots;
  }

  public function getCteStudentIndustryCredentials()
  {
     return $this->cteStudentIndustryCredentials;
  }

  public function setCteStudentIndustryCredentials($cteStudentIndustryCredentials)
  {
     $this->cteStudentIndustryCredentials = $cteStudentIndustryCredentials;
  }

  public function getCteStudentFacts()
  {
     return $this->cteStudentFacts;
  }

  public function setCteStudentFacts($cteStudentFacts)
  {
     $this->cteStudentFacts = $cteStudentFacts;
  }

  public function getDistrictSnapshots()
  {
     return $this->districtSnapshots;
  }

  public function setDistrictSnapshots($districtSnapshots)
  {
     $this->districtSnapshots = $districtSnapshots;
  }

  public function getProgramsFacts()
  {
     return $this->programsFacts;
  }

  public function setProgramsFacts($programsFacts)
  {
     $this->programsFacts = $programsFacts;
  }

  public function getPimsSchoolCalendars()
  {
     return $this->pimsSchoolCalendars;
  }

  public function setPimsSchoolCalendars($pimsSchoolCalendars)
  {
     $this->pimsSchoolCalendars = $pimsSchoolCalendars;
  }

  public function getSchoolEnrollments()
  {
     return $this->schoolEnrollments;
  }

  public function setSchoolEnrollments($schoolEnrollments)
  {
     $this->schoolEnrollments = $schoolEnrollments;
  }

  public function getSpecialEducationSnapshots()
  {
     return $this->specialEducationSnapshots;
  }

  public function setSpecialEducationSnapshots($specialEducationSnapshots)
  {
     $this->specialEducationSnapshots = $specialEducationSnapshots;
  }

  public function getStaffs()
  {
     return $this->staffs;
  }

  public function setStaffs($staffs)
  {
     $this->staffs = $staffs;
  }

  public function getStaffAssignments()
  {
     return $this->staffAssignments;
  }

  public function setStaffAssignments($staffAssignments)
  {
     $this->staffAssignments = $staffAssignments;
  }

  public function getStaffSnapshots()
  {
     return $this->staffSnapshots;
  }

  public function setStaffSnapshots($staffSnapshots)
  {
     $this->staffSnapshots = $staffSnapshots;
  }

  public function getStudents()
  {
     return $this->students;
  }

  public function setStudents($students)
  {
     $this->students = $students;
  }

  public function getPimStudentCalendarFacts()
  {
     return $this->pimStudentCalendarFacts;
  }

  public function setPimStudentCalendarFacts($pimStudentCalendarFacts)
  {
     $this->pimStudentCalendarFacts = $pimStudentCalendarFacts;
  }

  public function getStudentCourseEnrollments()
  {
     return $this->studentCourseEnrollments;
  }

  public function setStudentCourseEnrollments($studentCourseEnrollments)
  {
     $this->studentCourseEnrollments = $studentCourseEnrollments;
  }

  public function getStudentFacts()
  {
     return $this->studentFacts;
  }

  public function setStudentFacts($studentFacts)
  {
     $this->studentFacts = $studentFacts;
  }

  public function getStudentSnapshots()
  {
     return $this->studentSnapshots;
  }

  public function setStudentSnapshots($studentSnapshots)
  {
     $this->studentSnapshots = $studentSnapshots;
  }

  public function jsonSerialize() {
    return [
      'batchComments' => $this->batchComments,
      'courses' => $this->courses,
      'courseInstructorSnapshots' => $this->courseInstructorSnapshots,
      'cteStudentIndustryCredentials' => $this->cteStudentIndustryCredentials,
      'cteStudentFacts' => $this->cteStudentFacts,
      'districtSnapshots' => $this->districtSnapshots,
      'programsFacts' => $this->programsFacts,
      'pimsSchoolCalendars' => $this->pimsSchoolCalendars,
      'schoolEnrollments' => $this->schoolEnrollments,
      'specialEducationSnapshots' => $this->specialEducationSnapshots,
      'staffs' => $this->staffs,
      'staffAssignments' => $this->staffAssignments,
      'staffSnapshots' => $this->staffSnapshots,
      'students' => $this->students,
      'pimStudentCalendarFacts' => $this->pimStudentCalendarFacts,
      'studentCourseEnrollments' => $this->studentCourseEnrollments,
      'studentFacts' => $this->studentFacts,
      'studentSnapshots' => $this->studentSnapshots
    ];
  }
}

?>
