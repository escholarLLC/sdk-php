<?php namespace escholar\sdk\Requests;
use Exception;
use GuzzleHttp\Exception\ClientException;

class ApiRequest {
  private $apiClient;
  private $path;

  protected function __construct($apiClient, $path) {
    $this->apiClient = $apiClient;
    $this->path = $path;
  }

  protected function get($id = NULL) {
    $path = $this->path;

    if (isset($id)) {
      $path .= '/' . $id;
    }

    try {
      $response = $this->apiClient->client->request('GET', $path);
      return json_decode($response->getBody());

    } catch (ClientException $e) {
      $msg = "unknown error";
      $response = $e->getResponse();

      if ($response != NULL) {
        $msg = json_decode($e->getResponse()->getBody())->message;
      }

      throw new Exception($msg);
    }
  }

  protected function post($obj) {
    try {
      $response = $this->apiClient->client->request('POST', $this->path, [
        'json' => $obj
      ]);

      return json_decode($response->getBody());

    } catch (ClientException $e) {
      $msg = "unknown error";
      $response = $e->getResponse();

      if ($response != NULL) {
        $msg = json_decode($e->getResponse()->getBody())->message;
      }

      throw new Exception($msg);
    }
  }

}
?>
