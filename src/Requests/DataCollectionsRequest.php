<?php namespace escholar\sdk\Requests;

use escholar\sdk\Requests\ApiRequest;

class DataCollectionsRequest extends ApiRequest {
  const PATH = 'data-collections';

  public function __construct($apiClient) {
    parent::__construct($apiClient, self::PATH);
  }

  public function listDataCollections() {
    return parent::get();
  }
}

?>
