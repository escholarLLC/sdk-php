<?php namespace escholar\sdk\Requests;
require_once 'ApiRequest.php';

class DataFileGroupsRequest extends ApiRequest {

  public function __construct($apiClient, $dataCollectionId) {
    parent::__construct($apiClient, self::buildPath($dataCollectionId));
  }

  public function fetchDataFileGroup($dataFileGroupId) {
    return parent::get($dataFileGroupId);
  }

  public function createDataFileGroup($dataFileGroup) {
    return parent::post($dataFileGroup);
  }

  public static function buildPath($dataCollectionId) {
    return 'data-collections/' . $dataCollectionId . '/data-file-groups';
  }
}

?>
