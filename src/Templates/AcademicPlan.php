<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AcademicPlan implements JsonSerializable {

   public $districtCode;
   public $academicPlanCode;
   public $schoolYearDate;
   public $academicPlanType;
   public $academicPlanShortDescription;
   public $academicPlanLongDescription;
   public $academicPlanCategory;
   public $totalCreditsRequired;
   public $minimumGpaRequired;
   public $academicPlanSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
