<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AcademicStandards implements JsonSerializable {

   public $districtCode;
   public $standardUniqueIdentifier;
   public $documentTitle;
   public $documentEdition;
   public $standardStatementCode;
   public $standardGuid;
   public $standardStatementUri;
   public $standardUniqueIdentifierType;
   public $standardStatement;
   public $standardDisplayName;
   public $standardCategory;
   public $standardSubjectAreaCode;
   public $standardHierarchyLevel;
   public $standardHierarchyType;
   public $standardGradeLevelDescription;
   public $standardMinimumGradeLevelCode;
   public $standardMaximumGradeLevelCode;
   public $standardSortSequence;
   public $parentStandardUniqueIdentifier;
   public $alternateStandardStatement;
   public $standardSuccessCriteria;
   public $standardTestabilityTypeCode;
   public $standardLanguageCode;
   public $standardAgeDescription;
   public $standardDerivationComment;
   public $bloomsTaxonomyDomain;
   public $gardnerIntelligenceClassification;
   public $standardStatementPosition;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
