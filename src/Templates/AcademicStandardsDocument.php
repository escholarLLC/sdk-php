<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AcademicStandardsDocument implements JsonSerializable {

   public $districtCode;
   public $documentTitle;
   public $documentEdition;
   public $documentUri;
   public $documentSubjectArea;
   public $documentDataSource;
   public $documentPublicationDate;
   public $documentPublicationStatus;
   public $documentEffectiveStartDate;
   public $documentEffectiveEndDate;
   public $documentAuthor;
   public $documentDescription;
   public $documentLanguageCode;
   public $documentPublisher;
   public $documentJurisdiction;
   public $documentLicense;
   public $documentRights;
   public $documentRightsHolder;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
