<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentAccModCode implements JsonSerializable {

   public $districtCode;
   public $accommodationModificationCode;
   public $schoolYearDate;
   public $accModTypeCode;
   public $accModShortDesc;
   public $accModLongDesc;
   public $accModCategory;
   public $accModSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
