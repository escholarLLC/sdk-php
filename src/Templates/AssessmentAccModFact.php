<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentAccModFact implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $testDate;
   public $studentId;
   public $schoolYearDate;
   public $accommodationModificationCode;
   public $accModTypeCode;
   public $accOrModUsedIndicator;
   public $accOrModInIepIndicator;
   public $studentSnapshotDate;
   public $accModComment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
