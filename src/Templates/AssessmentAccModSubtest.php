<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentAccModSubtest implements JsonSerializable {

   public $districtCode;
   public $accommodationModificationCode;
   public $schoolYearDate;
   public $accModTypeCode;
   public $testDescription;
   public $subtestIdentifier;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
