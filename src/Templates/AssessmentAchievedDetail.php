<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentAchievedDetail implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $standardAchievedCode;
   public $standardAchieved;
   public $standardAchievedGroup;
   public $startOfRange;
   public $endOfRange;
   public $statePercentStudents;
   public $regionPercentStudents;
   public $districtPercentStudents;
   public $stateBenchmark;
   public $regionBenchmark;
   public $districtBenchmark;
   public $sortSequence1;
   public $sortSequence2;
   public $defaultValueIndicator;
   public $reportingStandardAchievedGroup;
   public $standardAchievedCodeAscendingRank;
   public $standardAchievedType;
   public $metStandardIndicator;
   public $standardAchievedShortDesc;
   public $assessmentStandardPerformanceLevel;
   public $scoreValueType;
   public $scoreType;
   public $standardAchievedVersion;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
