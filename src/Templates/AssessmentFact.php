<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentFact implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $testDate;
   public $studentId;
   public $testingLocationCode;
   public $alphaScore;
   public $numericScore;
   public $credits;
   public $nationalPercentile;
   public $localPercentile;
   public $masteryIndicator;
   public $assessmentStatus;
   public $assessmentLanguageCode;
   public $standardAchievedCode;
   public $normCurveEquiv;
   public $rawScore;
   public $scaleScore;
   public $percentScore;
   public $localStanine;
   public $nationalStanine;
   public $nationalPercentileByAge;
   public $numberOfItemsCorrect;
   public $objectiveMasteryScore;
   public $degreesOfReadingPower;
   public $intelligenceQuotient;
   public $standardPerformanceIndex;
   public $standardPerformanceLevel;
   public $gradeEquivalent;
   public $specialNormGroup;
   public $levelOfAggregation;
   public $testingModification;
   public $testAssignmentDate;
   public $evaluatorId;
   public $evaluatorName;
   public $schoolYearDate;
   public $subtestScaleScore;
   public $numberOfTimesTested;
   public $convertedScore;
   public $districtAccountabilityStatus;
   public $testingAccommodation;
   public $standardErrorOfMeasurement;
   public $scoringModelCode;
   public $surveyCompletionIndicator;
   public $schoolAccountabilityStatus;
   public $lexileMinimumScore;
   public $lexileMaximumScore;
   public $numberOfItemsAttempted;
   public $numberOfItemsOmitted;
   public $numberOfItemsIncorrect;
   public $associatedCourseIndicator;
   public $repeatIndicator;
   public $quantileScore;
   public $titleIStatus;
   public $testBookletId;
   public $holisticScore;
   public $stateAccountabilityStatus;
   public $testingLocationDistrictCode;
   public $accountableLocationCode;
   public $accountableLocationDistrictCode;
   public $studentSnapshotDate;
   public $regionalPercentile;
   public $testingEnvironmentCode;
   public $alternateStandardAchievedCode;
   public $studentGradeLevelCodeWhenAssessed;
   public $growthScore;
   public $studentMetStandardIndicator;
   public $studentMetAlternateStandardIndicator;
   public $primaryStandardAchievedScoreType;
   public $alternateStandardAchievedScoreType;
   public $eventCircumstanceCode;
   public $eventCircumstanceComment;
   public $raschScore;
   public $raschScaledScore;
   public $platformType;
   public $preSluggedTestBookletIndicator;
   public $timeTaken;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
