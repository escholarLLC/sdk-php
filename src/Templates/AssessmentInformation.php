<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentInformation implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $testDescriptionVersion;
   public $testType;
   public $testDescriptionLong;
   public $testGroup;
   public $testCategory;
   public $testVendor;
   public $testObjective;
   public $testDescriptionShort;
   public $testIdentifierOrganizationTypeCode;
   public $testPrimaryPurposeCode;
   public $testGuid;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
