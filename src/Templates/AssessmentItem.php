<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentItem implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $subtestSubjectArea;
   public $scoreType;
   public $subtestDescription;
   public $scoreTypeLong;
   public $scoreTypeNumeric;
   public $specialEdEvaluation;
   public $subtestName;
   public $subtestGradeSpan;
   public $subtestLevel;
   public $subtestForm;
   public $subtestAdministration;
   public $factIsATotal;
   public $resultsLevel1;
   public $resultsLevel2;
   public $resultsLevel3;
   public $resultsLevel4;
   public $stateAssessmentCode;
   public $subtestSortSequence;
   public $assessmentQualityRating;
   public $numberOfItems;
   public $standardErrorOfMeasurement;
   public $reportingTestDate;
   public $subtestMaximumScore;
   public $assessmentStudentSnapshotDate;
   public $assessmentStudentSnapshotPeriodLevel;
   public $testingWindowStartDate;
   public $testingWindowEndDate;
   public $assessmentRevisionDate;
   public $subtestShortName;
   public $subtestContentStandardCode;
   public $subtestVersionId;
   public $subtestCategory;
   public $subtestLowestGradeLevel;
   public $subtestHighestGradeLevel;
   public $subtestIdentifierOrganizationTypeCode;
   public $subtestPublishedDate;
   public $subtestPrimaryPurposeCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
