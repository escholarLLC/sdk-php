<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentItemLookup implements JsonSerializable {

   public $testDescription;
   public $testDate;
   public $subtestCode;
   public $levelCode;
   public $subtestGradeSpan;
   public $subtestLevel;
   public $subtestForm;
   public $subtestName;
   public $subtestSubjectArea;
   public $scoreType;
   public $factIsATotal;
   public $resultsLevel1;
   public $resultsLevel2;
   public $resultsLevel3;
   public $resultsLevel4;
   public $numberOfItems;
   public $standardErrorOfMeasurement;
   public $scoreTypeLong;
   public $subtestMaximumScore;
   public $testingWindowStartDate;
   public $testingWindowEndDate;
   public $assessmentRevisionDate;
   public $subtestShortName;
   public $subtestContentStandardCode;
   public $subtestVersionId;
   public $subtestCategory;
   public $subtestLowestGradeLevel;
   public $subtestHighestGradeLevel;
   public $subtestIdentifierOrganizationTypeCode;
   public $subtestPublishedDate;
   public $subtestPrimaryPurposeCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
