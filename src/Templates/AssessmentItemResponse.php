<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentItemResponse implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $itemResponseIdentifier;
   public $itemResponseGroup;
   public $itemResponseDetail;
   public $itemResponseType;
   public $itemResponseDetailDesc;
   public $maximumResponseValue;
   public $masteryResponseValue;
   public $correctResponseValue;
   public $stateLevel1Benchmark;
   public $stateLevel2Benchmark;
   public $stateLevel3Benchmark;
   public $stateLevel4Benchmark;
   public $stateSuccessRate;
   public $regionLevel1Benchmark;
   public $regionLevel2Benchmark;
   public $regionLevel3Benchmark;
   public $regionLevel4Benchmark;
   public $regionSuccessRate;
   public $itemResponseTypeLong;
   public $itemResponseDisplay;
   public $itemResponseGroupLong;
   public $itemResponseText;
   public $stateStandardCode;
   public $stateStandardDesc;
   public $primaryResponseScoreType;
   public $numberOfChoices;
   public $minimumResponseValue;
   public $itemResponseSubtestPercent;
   public $itemResponseLevel;
   public $itemResponseAllottedTime;
   public $alternateItemResponseDescription;
   public $itemResponseStimulusDescription;
   public $itemResponsePermissibleAids;
   public $itemResponseOmitIndicator;
   public $itemResponseReleasedIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
