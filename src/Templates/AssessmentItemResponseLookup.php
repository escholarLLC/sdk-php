<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentItemResponseLookup implements JsonSerializable {

   public $testDescription;
   public $testDate;
   public $subtestCode;
   public $levelCode;
   public $columnId;
   public $subtestGradeSpan;
   public $subtestLevel;
   public $subtestForm;
   public $itemResponseGroupLong;
   public $itemResponseDetailDesc;
   public $itemResponseDetail;
   public $itemResponseType;
   public $maximumResponseValue;
   public $masteryResponseValue;
   public $correctResponseValue;
   public $itemResponseText;
   public $stateLevel1Benchmark;
   public $stateLevel2Benchmark;
   public $stateLevel3Benchmark;
   public $stateLevel4Benchmark;
   public $stateSuccessRate;
   public $regionLevel1Benchmark;
   public $regionLevel2Benchmark;
   public $regionLevel3Benchmark;
   public $regionLevel4Benchmark;
   public $regionSuccessRate;
   public $levelOfAggregation;
   public $stateStandardCode;
   public $stateStandardDesc;
   public $primaryResponseScoreType;
   public $numberOfChoices;
   public $minimumResponseValue;
   public $itemResponseSubtestPercent;
   public $itemResponseAllottedTime;
   public $alternateItemResponseDescription;
   public $itemResponseStimulusDescription;
   public $itemResponsePermissibleAids;
   public $itemResponseOmitIndicator;
   public $itemResponseReleasedIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
