<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentResponse implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $testDate;
   public $studentId;
   public $itemResponseIdentifier;
   public $alphaValue;
   public $numericValue;
   public $levelOfAggregation;
   public $schoolYearDate;
   public $testingLocationCode;
   public $standardAchievedCode;
   public $staffId;
   public $percentScore;
   public $rawScore;
   public $scaleScore;
   public $numberOfItemsCorrect;
   public $testingLocationDistrictCode;
   public $studentSnapshotDate;
   public $responseStatus;
   public $responseSequenceNumber;
   public $analyzedResultStatus;
   public $studentMetStandardIndicator;
   public $standardAchievedScoreType;
   public $responseTimeTaken;
   public $responseFeedback;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
