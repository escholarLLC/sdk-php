<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentScoringModel implements JsonSerializable {

   public $scoringModelCode;
   public $schoolYearDate;
   public $districtCode;
   public $scoringModelName;
   public $scoringModelCategory;
   public $scoringModelDesc;
   public $scoringModelSortSequence;
   public $scoringModelCategorySortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
