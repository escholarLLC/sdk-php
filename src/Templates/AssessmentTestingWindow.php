<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssessmentTestingWindow implements JsonSerializable {

   public $districtCode;
   public $testingWindowCode;
   public $schoolYearDate;
   public $testingWindowShortDescription;
   public $testingWindowLongDescription;
   public $testingWindowStartDate;
   public $testingWindowEndDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
