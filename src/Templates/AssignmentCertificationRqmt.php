<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssignmentCertificationRqmt implements JsonSerializable {

   public $districtCode;
   public $assignmentCode;
   public $schoolYearDate;
   public $requirementGroup;
   public $certificationTypeCode;
   public $certificationAreaCode;
   public $certificationLevelCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
