<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssmntItemrAchievedDetail implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $itemResponseIdentifier;
   public $scoreType;
   public $standardAchievedCode;
   public $standardAchievedShortDesc;
   public $standardAchieved;
   public $startOfRange;
   public $endOfRange;
   public $metStandardIndicator;
   public $scoreValueType;
   public $assessmentStandardPerformanceLevel;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
