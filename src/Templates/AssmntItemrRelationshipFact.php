<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssmntItemrRelationshipFact implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $parentItemResponseIdentifier;
   public $childItemResponseIdentifier;
   public $relationshipType;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
