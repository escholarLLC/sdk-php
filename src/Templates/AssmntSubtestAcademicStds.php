<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AssmntSubtestAcademicStds implements JsonSerializable {

   public $assessmentDistrictCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $documentTitle;
   public $documentEdition;
   public $standardUniqueIdentifier;
   public $academicStandardsDistrictCode;
   public $submittingDistrictCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
