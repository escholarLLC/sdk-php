<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class AttendanceCodes implements JsonSerializable {

   public $districtCode;
   public $attendanceCode;
   public $attendanceDescription;
   public $attendanceCategory;
   public $attendanceCodeLong;
   public $attendanceType;
   public $sortSequence;
   public $attendanceStatus;
   public $stateAttendanceCode;
   public $stateAttendanceDescription;
   public $schoolYearDate;
   public $attendanceCodeType;
   public $attendanceShortDescription;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
