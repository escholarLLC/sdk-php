<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class Award implements JsonSerializable {

   public $districtCode;
   public $awardCode;
   public $schoolYearDate;
   public $awardType;
   public $awardShortDescription;
   public $awardLongDescription;
   public $awardDistinctionTypeCode;
   public $awardCategory;
   public $awardTitle;
   public $awardSortSequence;
   public $alternateAwardCategory;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
