<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class Building implements JsonSerializable {

   public $controllingDistrictCode;
   public $buildingId;
   public $buildingName;
   public $streetAddress1;
   public $streetAddress2;
   public $streetAddress3;
   public $city;
   public $stateCode;
   public $baseZipCode;
   public $zipCode4;
   public $stateCountyCode;
   public $siteIdentifier;
   public $siteLongitude;
   public $siteLatitude;
   public $yearOfOriginalConstruction;
   public $basicStructureTypeCode;
   public $basementIndicator;
   public $ansiCountyCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
