<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class BuildingComponent implements JsonSerializable {

   public $controllingDistrictCode;
   public $buildingId;
   public $schoolYearDate;
   public $buildingComponentCode;
   public $buildingComponentCategoryCode;
   public $buildingComponentCount;
   public $buildingComponentConditionCode;
   public $yearOfLastRenovation;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
