<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class BuildingHistory implements JsonSerializable {

   public $controllingDistrictCode;
   public $buildingId;
   public $schoolYearDate;
   public $buildingOwnerCode;
   public $yearOfLastFullModernization;
   public $numberOfStories;
   public $numberOfRooms;
   public $studentCapacity;
   public $buildingSiteAcreage;
   public $buildingGrossSquareFootage;
   public $floodPlainIndicator;
   public $wheelchairAccessibleIndicator;
   public $leasedOutIndicator;
   public $buildingDesignationCode;
   public $buildingConstructionHistoryDescription;
   public $buildingComplianceStatusCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
