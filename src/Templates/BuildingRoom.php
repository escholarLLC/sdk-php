<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class BuildingRoom implements JsonSerializable {

   public $controllingDistrictCode;
   public $buildingId;
   public $schoolYearDate;
   public $roomId;
   public $roomCapacity;
   public $roomGrossSquareFootage;
   public $roomLevelCode;
   public $temporaryRoomIndicator;
   public $roomYearOfOriginalConstruction;
   public $roomComplianceStatusCode;
   public $optimumRoomCapacity;
   public $roomDescription;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
