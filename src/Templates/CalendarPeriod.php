<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CalendarPeriod implements JsonSerializable {

   public $districtCode;
   public $periodStartDate;
   public $periodEndDate;
   public $periodLevel;
   public $schoolMonthDate;
   public $schoolYearDate;
   public $reportingPeriodShortDesc;
   public $dateDescription;
   public $weekNumber;
   public $monthNumber;
   public $dayOfWeek;
   public $monthName;
   public $dayName;
   public $reportSchoolYear;
   public $periodSortSequence;
   public $periodDescriptionShort;
   public $periodDescriptionLong;
   public $collectionCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
