<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CharterAuthorizer implements JsonSerializable {

   public $charterAuthorizerId;
   public $schoolYearDate;
   public $charterAuthorizerName;
   public $charterAuthorizerTypeCode;
   public $locationAddress1;
   public $locationAddress2;
   public $locationAddress3;
   public $locationAddressCity;
   public $locationAddressStateCode;
   public $locationAddressPostalCode;
   public $locationAddressZipCode4;
   public $mailingAddress1;
   public $mailingAddress2;
   public $mailingAddress3;
   public $mailingAddressCity;
   public $mailingAddressStateCode;
   public $mailingAddressPostalCode;
   public $mailingAddressZipCode4;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
