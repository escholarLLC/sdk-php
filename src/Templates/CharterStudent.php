<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CharterStudent implements JsonSerializable {

   public $charterSchoolDistrictCode;
   public $schoolYearDate;
   public $studentId;
   public $charterEnrollmentPeriodCode;
   public $districtCodeOfResidence;
   public $reportingDate;
   public $studentTypeCode;
   public $studentHomeAddress1;
   public $studentHomeAddress2;
   public $studentHomeAddressCity;
   public $studentHomeAddressStateCode;
   public $studentHomeAddressPostalCode;
   public $currentIepDate;
   public $priorIepDate;
   public $daysEnrolledInRegularEducation;
   public $daysEnrolledInSpecialEducation;
   public $daysInSession;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
