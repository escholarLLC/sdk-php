<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CharterTransaction implements JsonSerializable {

   public $charterSchoolDistrictCode;
   public $districtCodeOfResidence;
   public $transactionId;
   public $schoolYearDate;
   public $charterEnrollmentPeriodCode;
   public $transactionDate;
   public $transactionTypeCode;
   public $transactionAmount;
   public $transactionComment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
