<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class ClassAssessment implements JsonSerializable {

   public $districtCode;
   public $courseLocationCode;
   public $courseCode;
   public $courseSchoolYearDate;
   public $supplementaryCourseDifferentiator;
   public $sectionCode;
   public $courseInstructorSnapshotDate;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
