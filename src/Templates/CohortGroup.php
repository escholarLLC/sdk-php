<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CohortGroup implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $cohortGroupId;
   public $schoolYearDate;
   public $cohortGroupShortDescription;
   public $cohortGroupLongDescription;
   public $cohortGroupCategory;
   public $cohortGroupScope;
   public $cohortGroupSubjectAreaCode;
   public $cohortGroupPrimaryProgramsCode;
   public $alternateCohortGroupCategory;
   public $cohortGroupGenderCompositionCode;
   public $cohortGroupSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
