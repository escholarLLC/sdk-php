<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class Contact implements JsonSerializable {

   public $districtCode;
   public $primaryContactId;
   public $schoolYearDate;
   public $stateContactId;
   public $localContactId;
   public $statePhotoCardId;
   public $alternateContactId;
   public $contactNamePrefix;
   public $contactFirstName;
   public $contactMiddleName;
   public $contactLastName;
   public $contactNameSuffix;
   public $contactGenderCode;
   public $contactOralLanguageCode;
   public $contactWrittenLanguageCode;
   public $contactHomeAddress1;
   public $contactHomeAddress2;
   public $contactHomeAddress3;
   public $contactHomeCity;
   public $contactHomeStateCode;
   public $contactHomePostalCode;
   public $contactHomeStateCountyName;
   public $contactHomeCountryCode;
   public $contactMailingAddress1;
   public $contactMailingAddress2;
   public $contactMailingAddress3;
   public $contactMailingCity;
   public $contactMailingStateCode;
   public $contactMailingPostalCode;
   public $contactMailingStateCountyName;
   public $contactMailingCountryCode;
   public $contactHomePhoneNumber;
   public $contactWorkPhoneNumber;
   public $contactMobilePhoneNumber;
   public $contactWorkEmailAddress;
   public $contactPersonalEmailAddress;
   public $contactOccupationCode;
   public $personalInformationVerificationCode;
   public $contactBirthDate;
   public $highestEducationLevelCode;
   public $healthInsuranceStatusCode;
   public $dentalInsuranceStatusCode;
   public $primaryFamilyId;
   public $includedInFamilySizeCountIndicator;
   public $contactMilitaryStatusCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
