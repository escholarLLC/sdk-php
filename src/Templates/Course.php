<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class Course implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $courseCode;
   public $courseName;
   public $courseDepartment;
   public $subjectAreaCode;
   public $courseLength;
   public $numberOfCredits;
   public $numberOfClassSections;
   public $honorsIndicator;
   public $minimumNumberOfSeats;
   public $primaryCourseTypeCode;
   public $courseGradeTypeCode;
   public $numberOfGrades;
   public $courseWeight;
   public $additionalData;
   public $specialProgramIndicator;
   public $courseSpecialProgramCode;
   public $courseShortName;
   public $allowableCourseGender;
   public $gpaApplicabilityCode;
   public $supplementaryCourseDifferentiator;
   public $courseCodeLong;
   public $courseDeliveryModelCode;
   public $alternateCourseCode;
   public $graduationRequirementCode;
   public $courseDescription;
   public $dualCreditIndicator;
   public $advancedPlacementIndicator;
   public $careerAndTechnicalIndicator;
   public $giftedIndicator;
   public $englishLanguageLearnerIndicator;
   public $remedialIndicator;
   public $basicIndicator;
   public $specialEducationIndicator;
   public $internationalBaccalaureateIndicator;
   public $coreIndicator;
   public $electiveIndicator;
   public $nonTraditionalGenderCode;
   public $minimumGradeLevel;
   public $maximumGradeLevel;
   public $courseDescriptionLong;
   public $maximumNumberOfSeats;
   public $courseGroupCode;
   public $alternateCourseCode2;
   public $alternateCourseCode3;
   public $courseSessionTypeCode;
   public $courseCreditTypeCode;
   public $labComponentIndicator;
   public $optimumNumberOfSeats;
   public $advancedIndicator;
   public $primaryInstructionTypeCode;
   public $primaryInstructionDeliveryMethodCode;
   public $primaryInstructionMediumTypeCode;
   public $courseLevelCode;
   public $catalogCourseCode;
   public $stateStandardsAlignedIndicator;
   public $curriculumFrameworkTypeCode;
   public $endOfCourseExamRequiredCode;
   public $courseSequenceCode;
   public $numberOfParts;
   public $organizationDefiningCourse;
   public $minimumNumberOfCredits;
   public $maximumNumberOfCredits;
   public $generalIndicator;
   public $collegeLevelIndicator;
   public $untrackedIndicator;
   public $hsEquivalentIndicator;
   public $correspondenceIndicator;
   public $distanceLearningIndicator;
   public $graduationCreditIndicator;
   public $magnetIndicator;
   public $preAdvancedPlacementIndicator;
   public $preInternationalBaccalaureateIndicator;
   public $otherCharacteristicIndicator;
   public $ncaaEligibilityIndicator;
   public $careerClusterCode;
   public $abilityGroupingIndicator;
   public $primaryFundSourceCode;
   public $timeRequiredForCompletion;
   public $additionalCreditTypeCode;
   public $academicIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
