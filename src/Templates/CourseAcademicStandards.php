<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CourseAcademicStandards implements JsonSerializable {

   public $courseDistrictCode;
   public $locationCode;
   public $courseCode;
   public $schoolYearDate;
   public $supplementaryCourseDifferentiator;
   public $documentTitle;
   public $documentEdition;
   public $standardUniqueIdentifier;
   public $academicStandardsDistrictCode;
   public $submittingDistrictCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
