<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CourseCatalog implements JsonSerializable {

   public $districtCode;
   public $catalogCourseCode;
   public $schoolYearDate;
   public $courseName;
   public $courseDescriptionLong;
   public $courseLevelCode;
   public $courseSubjectAreaCode;
   public $graduationRequirementCode;
   public $gpaApplicabilityCode;
   public $minimumAvailableCredit;
   public $maximumAvailableCredit;
   public $courseCreditTypeCode;
   public $curriculumFrameworkTypeCode;
   public $courseSequenceCode;
   public $courseShortName;
   public $primaryCourseTypeCode;
   public $creditRangeDescription;
   public $coreIndicator;
   public $honorsIndicator;
   public $advancedPlacementIndicator;
   public $giftedIndicator;
   public $englishLanguageLearnerIndicator;
   public $remedialIndicator;
   public $basicIndicator;
   public $specialEducationIndicator;
   public $internationalBaccalaureateIndicator;
   public $generalIndicator;
   public $collegeLevelIndicator;
   public $untrackedIndicator;
   public $hsEquivalentIndicator;
   public $careerAndTechnicalIndicator;
   public $correspondenceIndicator;
   public $distanceLearningIndicator;
   public $graduationCreditIndicator;
   public $magnetIndicator;
   public $preAdvancedPlacementIndicator;
   public $preInternationalBaccalaureateIndicator;
   public $otherCharacteristicIndicator;
   public $courseGroupCode;
   public $academicIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
