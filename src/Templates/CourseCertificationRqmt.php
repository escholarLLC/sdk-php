<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CourseCertificationRqmt implements JsonSerializable {

   public $districtCode;
   public $courseCode;
   public $locationCode;
   public $schoolYearDate;
   public $supplementaryCourseDifferentiator;
   public $requirementGroup;
   public $certificationTypeCode;
   public $certificationAreaCode;
   public $certificationLevelCode;
   public $requirementGroup2;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
