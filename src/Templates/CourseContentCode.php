<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CourseContentCode implements JsonSerializable {

   public $districtCode;
   public $courseContentCode;
   public $courseContentDesc;
   public $courseContentCategory;
   public $sortSequence;
   public $schoolYearDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
