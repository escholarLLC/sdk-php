<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CourseDeliveryModel implements JsonSerializable {

   public $districtCode;
   public $courseDeliveryModelCode;
   public $courseDeliveryModelDesc;
   public $courseDeliveryModelCategory;
   public $sortSequence;
   public $schoolYearDate;
   public $courseInteractionMode;
   public $alternateCourseDeliveryModelCategory;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
