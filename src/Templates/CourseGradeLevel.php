<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CourseGradeLevel implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $courseCode;
   public $schoolYearDate;
   public $supplementaryCourseDifferentiator;
   public $gradeLevelCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
