<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CourseInstructorAssignment implements JsonSerializable {

   public $courseDistrictCode;
   public $courseLocationCode;
   public $schoolYearDate;
   public $courseCode;
   public $supplementaryCourseDifferentiator;
   public $sectionCode;
   public $termCode;
   public $markingPeriodCode;
   public $instructorDistrictCode;
   public $instructorId;
   public $instructorStartDate;
   public $instructorEndDate;
   public $primaryInstructorIndicator;
   public $primarySpecialEdInstructorIndicator;
   public $instructorTypeCode;
   public $primaryInstructionDeliveryMethodCode;
   public $primaryEnlInstructorIndicator;
   public $primaryInstructionLanguageCode;
   public $alternateInstructionLanguageCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
