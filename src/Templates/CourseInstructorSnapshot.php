<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CourseInstructorSnapshot implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $courseCode;
   public $sectionCode;
   public $roomAssignment;
   public $primaryInstructorId;
   public $additionalInstructor1Id;
   public $additionalInstructor2Id;
   public $additionalInstructor3Id;
   public $additionalInstructor4Id;
   public $additionalInstructor5Id;
   public $supplementaryCourseDifferentiator;
   public $classPeriod;
   public $courseCodeLong;
   public $primaryInstructionLanguageCode;
   public $alternateInstructionLanguageCode;
   public $sectionCodeLong;
   public $secondaryContentIndicator;
   public $snapshotDate;
   public $additionalInstructor1TypeCode;
   public $additionalInstructor2TypeCode;
   public $additionalInstructor3TypeCode;
   public $additionalInstructor4TypeCode;
   public $additionalInstructor5TypeCode;
   public $primaryInstructorHqStatusCode;
   public $additionalInstructor1HqStatusCode;
   public $additionalInstructor2HqStatusCode;
   public $additionalInstructor3HqStatusCode;
   public $additionalInstructor4HqStatusCode;
   public $additionalInstructor5HqStatusCode;
   public $regularEducationStudentCount;
   public $specialEducationStudentCount;
   public $giftedStudentCount;
   public $totalStudentCount;
   public $regularEducationClassMakeupCode;
   public $specialEducationClassMakeupCode;
   public $giftedClassMakeupCode;
   public $scheduledMinutesPerClassSession;
   public $actualMinutesPerClassSession;
   public $scheduledClassSessionsPerYear;
   public $actualClassSessionsPerYear;
   public $primaryInstructorControllingDistrictCode;
   public $additionalInstructor1ControllingDistrictCode;
   public $additionalInstructor2ControllingDistrictCode;
   public $additionalInstructor3ControllingDistrictCode;
   public $additionalInstructor4ControllingDistrictCode;
   public $additionalInstructor5ControllingDistrictCode;
   public $primaryInstrClassResponsibilityWeight;
   public $additionalInstr1ClassResponsibilityWeight;
   public $additionalInstr2ClassResponsibilityWeight;
   public $additionalInstr3ClassResponsibilityWeight;
   public $additionalInstr4ClassResponsibilityWeight;
   public $additionalInstr5ClassResponsibilityWeight;
   public $numberOfCredits;
   public $courseSpecialProgramCode;
   public $allowableClassGender;
   public $courseDeliveryModelCode;
   public $gpaApplicabilityCode;
   public $honorsIndicator;
   public $advancedPlacementIndicator;
   public $careerAndTechnicalIndicator;
   public $giftedIndicator;
   public $englishLanguageLearnerIndicator;
   public $remedialIndicator;
   public $basicIndicator;
   public $specialEducationIndicator;
   public $internationalBaccalaureateIndicator;
   public $coreIndicator;
   public $electiveIndicator;
   public $dualCreditIndicator;
   public $minimumGradeLevel;
   public $maximumGradeLevel;
   public $labComponentIndicator;
   public $minimumNumberOfSeats;
   public $optimumNumberOfSeats;
   public $maximumNumberOfSeats;
   public $graduationRequirementCode;
   public $advancedIndicator;
   public $primaryInstructionTypeCode;
   public $primaryInstructionDeliveryMethodCode;
   public $primaryInstructionMediumTypeCode;
   public $instructionalSettingCode;
   public $classMakeupCode;
   public $courseSequenceCode;
   public $buildingDistrictCode;
   public $buildingId;
   public $markingPeriodCode;
   public $termCode;
   public $attendanceTrackingCode;
   public $primaryInstructorStartDate;
   public $additionalInstructor1StartDate;
   public $additionalInstructor2StartDate;
   public $additionalInstructor3StartDate;
   public $additionalInstructor4StartDate;
   public $additionalInstructor5StartDate;
   public $primaryInstructorEndDate;
   public $additionalInstructor1EndDate;
   public $additionalInstructor2EndDate;
   public $additionalInstructor3EndDate;
   public $additionalInstructor4EndDate;
   public $additionalInstructor5EndDate;
   public $abilityGroupingIndicator;
   public $classGradeTypeCode;
   public $excludeFromEvaluationIndicator;
   public $timeRequiredForCompletion;
   public $primaryInstructorTypeCode;
   public $className;
   public $homeroomIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
