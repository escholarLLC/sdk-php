<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CreditGpaCode implements JsonSerializable {

   public $districtCode;
   public $creditGpaCode;
   public $schoolYearDate;
   public $creditGpaShortDescription;
   public $creditGpaLongDescription;
   public $creditGpaCategory;
   public $creditGpaStatus;
   public $creditGpaRequiredIndicator;
   public $creditGpaRequirementGroupId;
   public $creditGpaSortSequence;
   public $gpaRangeMinimum;
   public $gpaRangeMaximum;
   public $creditGpaSubjectAreaCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
