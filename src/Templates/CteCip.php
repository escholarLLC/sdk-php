<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CteCip implements JsonSerializable {

   public $districtCode;
   public $cipCode;
   public $deliveryMethodCode;
   public $schoolYearDate;
   public $cipDescription;
   public $careerClusterCode;
   public $cipSortSequence;
   public $nonTraditionalGenderCode;
   public $programCompletionHours;
   public $cipCategory;
   public $cipEditionStartYear;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
