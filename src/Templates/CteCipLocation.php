<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CteCipLocation implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $cipCode;
   public $deliveryMethodCode;
   public $schoolYearDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
