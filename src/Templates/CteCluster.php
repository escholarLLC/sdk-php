<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CteCluster implements JsonSerializable {

   public $districtCode;
   public $cteClusterCode;
   public $deliveryMethodCode;
   public $schoolYearDate;
   public $cteClusterDescription;
   public $cteClusterCategory;
   public $cteClusterSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
