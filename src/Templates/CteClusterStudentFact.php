<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CteClusterStudentFact implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $cteLocationCode;
   public $studentLocationCode;
   public $cteClusterCode;
   public $deliveryMethodCode;
   public $reportingDate;
   public $cteParticipantTypeCode;
   public $cteConcentratorTypeCode;
   public $secondarySchoolCompletionIndicator;
   public $includedInGraduationRateIndicator;
   public $postSchoolMilitaryServiceIndicator;
   public $postSchoolEmploymentIndicator;
   public $postSchoolFurtherStudyIndicator;
   public $postSchoolAdvancedTrainingIndicator;
   public $cteLocationDistrictCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
