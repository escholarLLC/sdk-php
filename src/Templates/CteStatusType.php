<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CteStatusType implements JsonSerializable {

   public $districtCode;
   public $cteStatusTypeCode;
   public $schoolYearDate;
   public $cteStatusTypeDescription;
   public $cteStatusTypeShortDescription;
   public $minimumProgramCompletionLevel;
   public $maximumProgramCompletionLevel;
   public $cteStatusTypeSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
