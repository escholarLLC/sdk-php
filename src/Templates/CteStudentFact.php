<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CteStudentFact implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $cteCipLocationCode;
   public $studentLocationCode;
   public $cipCode;
   public $deliveryMethodCode;
   public $reportingDate;
   public $reportingDatePeriodLevel;
   public $cteStatusTypeCode;
   public $cteProgramCompletionPlanCode;
   public $registeredApprenticeshipIndicator;
   public $internshipIndicator;
   public $cooperativeWorkIndicator;
   public $jobExplorationIndicator;
   public $agricultureExperienceIndicator;
   public $schoolSponsoredEnterpriseIndicator;
   public $numberOfProgramHoursCompleted;
   public $percentageOfProgramCompleted;
   public $cumulativePostsecondaryCreditsEarned;
   public $cteProgramContinuationIndicator;
   public $workBasedExperienceIndicator;
   public $cteFormOnFileIndicator;
   public $skillAttainmentMethodCode;
   public $cteProgramPerformanceCode;
   public $pellGrantIndicator;
   public $academicInstructionCode;
   public $includedInGraduationRateIndicator;
   public $cteCipLocationDistrictCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
