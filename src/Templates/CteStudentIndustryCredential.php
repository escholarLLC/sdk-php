<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class CteStudentIndustryCredential implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $studentLocationCode;
   public $cipCode;
   public $deliveryMethodCode;
   public $industryCredentialCode;
   public $credentialEarnedDate;
   public $credentialEarnedPeriodLevel;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
