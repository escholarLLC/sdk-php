<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class DayCalendar implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $schoolDate;
   public $locationGradeLevel;
   public $duration;
   public $dayType;
   public $dayStatus;
   public $dayNumber;
   public $schoolMonthNumber;
   public $schoolMonthName;
   public $instructionalMinutesInDay;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
