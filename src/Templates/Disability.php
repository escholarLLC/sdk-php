<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class Disability implements JsonSerializable {

   public $districtCode;
   public $disabilityCode;
   public $schoolYearDate;
   public $disabilityShortDescription;
   public $disabilityLongDescription;
   public $disabilityCategory;
   public $standardizedDisabilityDescription;
   public $disabilitySortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
