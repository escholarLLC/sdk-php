<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class DisciplineInfractionCodes implements JsonSerializable {

   public $districtCode;
   public $infractionCode;
   public $infractionLongDescription;
   public $infractionCategory;
   public $infractionSeverity;
   public $infractionSortSequence;
   public $alternateInfractionCategory;
   public $stateInfractionCode;
   public $stateInfractionDescription;
   public $schoolYearDate;
   public $infractionShortDescription;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
