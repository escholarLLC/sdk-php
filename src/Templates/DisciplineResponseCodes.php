<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class DisciplineResponseCodes implements JsonSerializable {

   public $districtCode;
   public $responseCode;
   public $responseLongDescription;
   public $responseCategory;
   public $responseSortSequence;
   public $alternateResponseCategory;
   public $stateResponseCode;
   public $stateResponseDescription;
   public $schoolYearDate;
   public $responseShortDescription;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
