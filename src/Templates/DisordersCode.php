<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class DisordersCode implements JsonSerializable {

   public $districtCode;
   public $disorderCode;
   public $schoolYearDate;
   public $disorderDesc;
   public $disorderType;
   public $stateDisorderCode;
   public $stateDisorderDesc;
   public $disorderSortSequence;
   public $disorderCategory;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
