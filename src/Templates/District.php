<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class District implements JsonSerializable {

   public $districtKey;
   public $districtCode;
   public $localDistrictId;
   public $districtName;
   public $districtStateAbbreviation;
   public $districtStateName;
   public $stateDistrictId;
   public $organizationTypeCode;
   public $securityContactName;
   public $securityContactPhone;
   public $securityContactEmail;
   public $analyticsContactName;
   public $analyticsContactPhone;
   public $analyticsContactEmail;
   public $dataSupplyContactName;
   public $dataSupplyContactPhone;
   public $dataSupplyContactEmail;
   public $dataContentContactName;
   public $dataContentContactPhone;
   public $dataContentContactEmail;
   public $technicalContactName;
   public $technicalContactPhone;
   public $technicalContactEmail;
   public $countyName;
   public $tier1Organization;
   public $tier2Organization;
   public $superintendentName;
   public $districtInstitutionCode;
   public $stateCountyCode;
   public $districtWebSiteUrl;
   public $ncesLeaIdentifier;
   public $districtMapLink;
   public $districtLegalName;
   public $districtNameLong;
   public $districtAddress1;
   public $districtAddress2;
   public $districtAddress3;
   public $districtCity;
   public $districtZipCode;
   public $districtZipCode4;
   public $needToResourceCapacityCode;
   public $organizationSubtype;
   public $titleIDistrictStatusCode;
   public $districtOperationalStatusCode;
   public $lastStatusDate;
   public $districtOpenDate;
   public $districtCloseDate;
   public $districtCharterTypeCode;
   public $priorStateDistrictId;
   public $msaCode;
   public $metroStatusCode;
   public $districtReasonForClosure;
   public $stateDistrictCodeIndicator;
   public $residenceDistrictIndicator;
   public $fundingDistrictIndicator;
   public $alternateTier1Organization;
   public $regionalOrganizationCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
