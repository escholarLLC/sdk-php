<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class DistrictFact implements JsonSerializable {

   public $districtCode;
   public $reportingDate;
   public $category01;
   public $category02;
   public $category03;
   public $primaryMeasureType;
   public $count;
   public $amount;
   public $percent;
   public $indicator;
   public $category04;
   public $category05;
   public $category06;
   public $category07;
   public $category08;
   public $category09;
   public $category10;
   public $categorySetCode;
   public $startDate;
   public $endDate;
   public $comment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
