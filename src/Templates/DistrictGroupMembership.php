<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class DistrictGroupMembership implements JsonSerializable {

   public $districtCode;
   public $reportingDate;
   public $entityGroupType;
   public $entityGroupValue;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
