<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class DistrictMarkingPeriod implements JsonSerializable {

   public $districtCode;
   public $markingPeriodCode;
   public $schoolYearDate;
   public $markingPeriodStartDate;
   public $markingPeriodEndDate;
   public $districtMarkingPeriodDescription;
   public $termCode;
   public $instructionalDays;
   public $instructionalMinutes;
   public $districtTermCategory;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
