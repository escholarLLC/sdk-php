<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class DistrictSnapshot implements JsonSerializable {

   public $districtCode;
   public $reportingDate;
   public $districtLegalName;
   public $congressionalDistrictNumber;
   public $stateLegislativeDistrictNumber;
   public $dunsNumber;
   public $ncesLeaIdentifier;
   public $ansiCountyCode;
   public $tier1Organization;
   public $tier2Organization;
   public $regionalOrganizationCode;
   public $districtLocale;
   public $locationLatitude;
   public $locationLongitude;
   public $locationAddress1;
   public $locationAddress2;
   public $locationAddress3;
   public $locationCity;
   public $locationStateCode;
   public $locationZipCode;
   public $locationZipCode4;
   public $mailingAddress1;
   public $mailingAddress2;
   public $mailingAddress3;
   public $mailingCity;
   public $mailingStateCode;
   public $mailingZipCode;
   public $mailingZipCode4;
   public $agencyPhoneNumber;
   public $alternateTelephoneNumber;
   public $faxTelephoneNumber;
   public $administratorFirstName;
   public $administratorLastName;
   public $administratorMiddleName;
   public $administratorNameSuffix;
   public $administratorPositionTitle;
   public $administratorEmailAddress;
   public $districtAgencyType;
   public $districtOperationalStatus;
   public $districtMapLink;
   public $districtAypStatus;
   public $accreditationStatus;
   public $contractIndicator;
   public $followOnDistrictCode;
   public $ncesSupervisoryUnionIdNum;
   public $graduationRate;
   public $dropoutRate;
   public $mobilityRate;
   public $truancyRate;
   public $esmsAcademicIndicatorStatus;
   public $hsGraduationRateIndicatorStatus;
   public $improvementStatus;
   public $integratedTechnologyStatus;
   public $mathParticipationStatus;
   public $mathProficiencyTargetStatus;
   public $readingelaParticipationStatus;
   public $readingelaProficiencyTargetStatus;
   public $safeHarborApplicationStatus;
   public $dangerousSchoolCount;
   public $notDangerousSchoolCount;
   public $districtClass;
   public $countyName;
   public $stateCountyCode;
   public $districtNameLong;
   public $districtName;
   public $administratorName;
   public $needToResourceCapacityCode;
   public $organizationSubtype;
   public $districtInstitutionCode;
   public $titleIDistrictStatusCode;
   public $stateSupervisoryUnionIdNum;
   public $stateDistrictId;
   public $districtWebSiteUrl;
   public $lastStatusDate;
   public $districtOpenDate;
   public $districtCloseDate;
   public $districtCharterTypeCode;
   public $priorStateDistrictId;
   public $msaCode;
   public $metroStatusCode;
   public $districtReasonForClosure;
   public $administratorNamePrefix;
   public $gfsaReportSubmissionStatus;
   public $reapAlternateFundingIndicator;
   public $alternateApproachStatus;
   public $amaoLepProficiencyStatus;
   public $amaoLepProgressStatus;
   public $outOfStateDistrict;
   public $reapSrsaEligibilityIndicator;
   public $exitedImprovementStatusIndicator;
   public $residenceDistrictIndicator;
   public $fundingDistrictIndicator;
   public $districtInstructionStartDate;
   public $districtEntryMinimumAge;
   public $districtEntryCutoffDate;
   public $kindergartenProgramTypeCode;
   public $schoolChoiceImplementationStatusCode;
   public $lastAypAppealProcessDate;
   public $harassmentAndBullyingPolicyIndicator;
   public $desegregationPlanIndicator;
   public $districtCategoryCode;
   public $intermediateEducationalUnitId;
   public $parentLocalEducationAgencyDistrictCode;
   public $policyComplianceCode;
   public $alternateTier1Organization;
   public $appealedAypDesignationIndicator;
   public $aypAppealChangedDesignationIndicator;
   public $giftedEligibilityCode;
   public $harassmentAndBullyingPolicyWebSiteUrl;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
