<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class DqeRules implements JsonSerializable {

   public $schoolYearDate;
   public $ruleId;
   public $field1Value;
   public $field2Value;
   public $field3Value;
   public $field4Value;
   public $field5Value;
   public $field6Value;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
