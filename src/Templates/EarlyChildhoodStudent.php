<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class EarlyChildhoodStudent implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $reportingDate;
   public $servicePriorityCode;
   public $previouslyServedIndicator;
   public $developmentalScreeningDate;
   public $healthAssessmentDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
