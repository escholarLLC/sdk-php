<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class EducationServices implements JsonSerializable {

   public $districtCode;
   public $serviceCode;
   public $schoolYearDate;
   public $serviceDesc;
   public $serviceShortDesc;
   public $serviceCategory;
   public $programServiceCode;
   public $stateServiceCode;
   public $stateServiceDesc;
   public $serviceSortSequence;
   public $serviceType;
   public $programsCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
