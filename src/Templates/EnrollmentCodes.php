<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class EnrollmentCodes implements JsonSerializable {

   public $districtCode;
   public $enrollmentCode;
   public $enrollmentCategory;
   public $enrollmentEffect;
   public $enrollmentDescription;
   public $alternateEnrollmentCategory;
   public $stateEnrollmentCode;
   public $stateEnrollmentDescription;
   public $enrollmentSortSequence;
   public $schoolYearDate;
   public $enrollmentCodeType;
   public $enrollmentCategoryType;
   public $enrollmentShortDescription;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
