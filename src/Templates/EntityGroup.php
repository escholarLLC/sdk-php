<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class EntityGroup implements JsonSerializable {

   public $entityGroupLevel;
   public $entityGroupType;
   public $entityGroupValue;
   public $entityGroupName;
   public $entityGroupDescription;
   public $entityGroupCategory;
   public $entityGroupSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
