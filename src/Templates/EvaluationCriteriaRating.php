<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class EvaluationCriteriaRating implements JsonSerializable {

   public $districtCode;
   public $evaluationCriteriaCode;
   public $evaluationCriteriaRatingCode;
   public $schoolYearDate;
   public $evaluationCriteriaShortDescription;
   public $evaluationCriteriaLongDescription;
   public $evaluationCriteriaCategory;
   public $evaluationCriteriaSortSequence;
   public $evaluationCriteriaRatingShortDescription;
   public $evaluationCriteriaRatingLongDescription;
   public $evaluationCriteriaRatingCategory;
   public $minimumEvaluationCriteriaRatingPoints;
   public $maximumEvaluationCriteriaRatingPoints;
   public $evaluationCriteriaRatingSortSequence;
   public $evaluationGroupCode;
   public $evaluationCriteriaRatingScale;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
