<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class EvaluationGroup implements JsonSerializable {

   public $districtCode;
   public $evaluationGroupCode;
   public $schoolYearDate;
   public $evaluationGroupShortDescription;
   public $evaluationGroupLongDescription;
   public $evaluationGroupCategory;
   public $alternateEvaluationGroupCategory;
   public $evaluationGroupSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
