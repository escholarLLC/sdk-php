<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class Family implements JsonSerializable {

   public $districtCode;
   public $familyId;
   public $schoolYearDate;
   public $familySize;
   public $householdSize;
   public $familyIncomeAmount;
   public $incomeCalculationMethodCode;
   public $primaryIncomeSourceCode;
   public $proofOfResidencyTypeCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
