<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class FiscalPeriod implements JsonSerializable {

   public $districtCode;
   public $fiscalPeriodStartDate;
   public $fiscalPeriodEndDate;
   public $fiscalPeriodLevel;
   public $schoolYearDate;
   public $fiscalPeriodDescription;
   public $fiscalPeriodNumber;
   public $reportSchoolYear;
   public $fiscalPeriodSortSequence;
   public $fiscalYearDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
