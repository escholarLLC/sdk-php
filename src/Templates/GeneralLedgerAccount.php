<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class GeneralLedgerAccount implements JsonSerializable {

   public $districtCode;
   public $accountCode;
   public $accountMajorCode;
   public $accountMinorCode;
   public $accountDescription;
   public $accountMajorDescription;
   public $accountMinorDescription;
   public $accountSubCode1;
   public $accountSubCode2;
   public $accountSubCode1Desc;
   public $accountSubCode2Desc;
   public $accountType;
   public $fiscalYearDate;
   public $accountName;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
