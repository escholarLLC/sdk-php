<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class GeneralLedgerDepartment implements JsonSerializable {

   public $districtCode;
   public $departmentCode;
   public $departmentName;
   public $departmentDescription;
   public $departmentCategory;
   public $departmentManager;
   public $departmentUnion;
   public $departmentSubjectArea;
   public $fiscalYearDate;
   public $applicableForBudgetIndicator;
   public $applicableForPayrollIndicator;
   public $applicableForActualIndicator;
   public $applicableForSsaIndicator;
   public $departmentCategory2;
   public $departmentCategory3;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
