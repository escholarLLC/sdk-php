<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class GeneralLedgerFact implements JsonSerializable {

   public $districtCode;
   public $fiscalYearDate;
   public $fiscalMonthNumber;
   public $glProgramCode;
   public $departmentCode;
   public $fundSourceCode;
   public $locationCode;
   public $objectCode;
   public $versionCode;
   public $glAccountCode;
   public $glAmount;
   public $glTransactionType;
   public $transactionMultiplier;
   public $vendorName;
   public $vendorId;
   public $annotation;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
