<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class GeneralLedgerFundSource implements JsonSerializable {

   public $districtCode;
   public $fundSourceCode;
   public $fundSourceName;
   public $fundSourceDescription;
   public $fundSourceManager;
   public $fundSourceCategory;
   public $fundSourceOriginationDate;
   public $fundSourceTerminationDate;
   public $fiscalYearDate;
   public $applicableForBudgetIndicator;
   public $applicableForPayrollIndicator;
   public $applicableForActualIndicator;
   public $applicableForSsaIndicator;
   public $fundSourceCategory2;
   public $fundSourceCategory3;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
