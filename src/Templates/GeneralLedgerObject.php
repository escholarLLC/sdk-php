<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class GeneralLedgerObject implements JsonSerializable {

   public $districtCode;
   public $objectCode;
   public $objectName;
   public $objectDescription;
   public $objectCategory;
   public $fiscalYearDate;
   public $applicableForBudgetIndicator;
   public $applicableForPayrollIndicator;
   public $applicableForActualIndicator;
   public $applicableForSsaIndicator;
   public $objectCategory2;
   public $objectCategory3;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
