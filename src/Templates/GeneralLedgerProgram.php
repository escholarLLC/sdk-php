<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class GeneralLedgerProgram implements JsonSerializable {

   public $districtCode;
   public $glProgramCode;
   public $glProgramName;
   public $glProgramDescription;
   public $glProgramCategory;
   public $glProgramManager;
   public $programsCode;
   public $glProgramOriginationDate;
   public $glProgramTerminationDate;
   public $fiscalYearDate;
   public $applicableForBudgetIndicator;
   public $applicableForPayrollIndicator;
   public $applicableForActualIndicator;
   public $applicableForSsaIndicator;
   public $applicableForStaffIndicator;
   public $glProgramNumber;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
