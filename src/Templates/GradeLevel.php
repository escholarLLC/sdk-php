<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class GradeLevel implements JsonSerializable {

   public $districtCode;
   public $gradeLevelCode;
   public $schoolYearDate;
   public $gradeLevelShortDescription;
   public $gradeLevelLongDescription;
   public $gradeLevelOrdinal;
   public $gradeLevelCategory;
   public $gradeLevelSortSequence;
   public $alternateGradeLevelCategory;
   public $standardizedGradeLevelDescription;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
