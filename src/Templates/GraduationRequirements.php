<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class GraduationRequirements implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $subjectAreaCode;
   public $timeRequired;
   public $graduationRequirementCode;
   public $gradRequirementsComment;
   public $totalCreditsRequired;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
