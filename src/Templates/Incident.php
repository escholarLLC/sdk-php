<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class Incident implements JsonSerializable {

   public $submittingDistrictCode;
   public $schoolYearDate;
   public $incidentId;
   public $incidentDate;
   public $locationCodeOfIncident;
   public $localIncidentId;
   public $incidentPlaceCode;
   public $incidentTimeFrameCode;
   public $incidentAgainstPropertyIndicator;
   public $incidentCost;
   public $drugRelatedIndicator;
   public $alcoholRelatedIndicator;
   public $gangRelatedIndicator;
   public $hateRelatedIndicator;
   public $seriousBodilyInjuryIndicator;
   public $primaryInfractionCode;
   public $incidentReporterTypeCode;
   public $incidentTime;
   public $incidentDescription;
   public $incidentReporterPersonId;
   public $incidentCostDescription;
   public $incidentManifestationCode;
   public $incidentPlaceCategoryCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
