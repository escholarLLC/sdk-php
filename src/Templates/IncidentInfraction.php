<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class IncidentInfraction implements JsonSerializable {

   public $submittingDistrictCode;
   public $incidentId;
   public $infractionCode;
   public $schoolYearDate;
   public $infractionRankOrSequence;
   public $infractionComment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
