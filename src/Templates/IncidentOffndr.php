<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class IncidentOffndr implements JsonSerializable {

   public $submittingDistrictCode;
   public $incidentId;
   public $offenderId;
   public $schoolYearDate;
   public $offenderType;
   public $ageAtTimeOfIncident;
   public $gradeLevelCodeAtTimeOfIncident;
   public $lleNotifiedIndicator;
   public $nameOfLleOfficeContacted;
   public $arrestedCode;
   public $adjudicationCode;
   public $assignedToAlternativeEducationIndicator;
   public $weaponDetectionMethodCode;
   public $weaponDetectionComment;
   public $criminalChargeIndicator;
   public $primaryDisabilityCode;
   public $injurySeverityCode;
   public $lepStatusIndicator;
   public $offenderEnrollmentLocationCode;
   public $offenderIncidentManifestationCode;
   public $sendingDistrictCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
