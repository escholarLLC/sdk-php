<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class IncidentOffndrDsplnryAction implements JsonSerializable {

   public $submittingDistrictCode;
   public $incidentId;
   public $offenderId;
   public $disciplinaryActionCode;
   public $schoolYearDate;
   public $disciplinaryActionStartDate;
   public $disciplinaryActionEndDate;
   public $actualDisciplinaryActionDuration;
   public $durationShortenedIndicator;
   public $receivedServicesIndicator;
   public $disciplinaryActionComment;
   public $originalDisciplinaryActionDuration;
   public $durationDifferenceReasonCode;
   public $disciplinaryActionDeterminationDate;
   public $relatedToZeroTolerancePolicyIndicator;
   public $disciplinaryActionId;
   public $responsibleLocationDistrictCode;
   public $responsibleLocationCode;
   public $disciplineAssignmentLocationDistrictCode;
   public $disciplineAssignmentLocationCode;
   public $primaryEnforcementStaffDistrictCode;
   public $primaryEnforcementStaffId;
   public $iepPlacementMeetingIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
