<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class IncidentOffndrInfrWeapon implements JsonSerializable {

   public $submittingDistrictCode;
   public $incidentId;
   public $offenderId;
   public $infractionCode;
   public $schoolYearDate;
   public $weaponCode;
   public $weaponCount;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
