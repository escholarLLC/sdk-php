<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class IncidentSecondaryBehavior implements JsonSerializable {

   public $submittingDistrictCode;
   public $schoolYearDate;
   public $incidentId;
   public $secondaryBehaviorCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
