<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class IncidentVictim implements JsonSerializable {

   public $submittingDistrictCode;
   public $incidentId;
   public $victimId;
   public $schoolYearDate;
   public $victimType;
   public $ageAtTimeOfIncident;
   public $gradeLevelCodeAtTimeOfIncident;
   public $studentAssistancePgmReferralIndicator;
   public $injurySeverityCode;
   public $medicalTreatmentRequiredIndicator;
   public $treatmentReasonCode;
   public $incidentVictimComment;
   public $transferOfferedIndicator;
   public $transferAcceptedIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
