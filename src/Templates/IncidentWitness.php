<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class IncidentWitness implements JsonSerializable {

   public $submittingDistrictCode;
   public $incidentId;
   public $witnessId;
   public $schoolYearDate;
   public $witnessType;
   public $incidentWitnessComment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
