<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class KeiAssessment implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestName;
   public $itemResponseDescription;
   public $studentId;
   public $testDate;
   public $subtestRawScore;
   public $subtestAchievementLevel;
   public $itemResponseRawScore;
   public $itemResponseAchievementLevel;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
