<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class Location implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $locationShortName;
   public $locationName;
   public $locationAddress1;
   public $locationAddress2;
   public $locationCity;
   public $locationStateCode;
   public $locationZipCode;
   public $locationPhoneNumber;
   public $schoolLevelCode;
   public $alternativeSchoolIndicator;
   public $alternativeSchoolDescription;
   public $locationArea;
   public $stateLocationId;
   public $principalName;
   public $locationStatusCode;
   public $studentEnrollmentAllowedIndicator;
   public $titleILocationStatusCode;
   public $gradeRangeLong;
   public $gradeRangeShort;
   public $countyName;
   public $responsibleReportingDistrictCode;
   public $financialReportingDistrictCode;
   public $geographicReportingDistrictCode;
   public $otherReportingDistrictCode;
   public $povertyPercentageLevel;
   public $locationInstitutionCode;
   public $locationLocale;
   public $locationLatitude;
   public $locationLongitude;
   public $magnetStatusCode;
   public $charterTypeCode;
   public $vocationalLocation;
   public $locationWebSiteUrl;
   public $reportPrompt;
   public $ncesSchoolIdentifier;
   public $testingServiceSchoolNumber;
   public $stateCountyCode;
   public $locationLegalName;
   public $locationAddress3;
   public $tier1Organization;
   public $tier2Organization;
   public $parentInstitutionCode;
   public $needToResourceCapacityCode;
   public $organizationSubtype;
   public $organizationTypeCode;
   public $parentDistrictName;
   public $locationSortSequence;
   public $virtualLocation;
   public $homeInstructionLocation;
   public $homeBoundLocation;
   public $outOfDistrictLocation;
   public $inDistrictLocation;
   public $ieuLocation;
   public $governmentInstitutionLocation;
   public $administrativeFundingControlCode;
   public $mailableLocation;
   public $sharedLocation;
   public $lastStatusDate;
   public $locationOpenDate;
   public $locationCloseDate;
   public $locationReasonForClosure;
   public $schoolCategoryCode;
   public $vendorLocation;
   public $regularLocation;
   public $ansiCountyCode;
   public $locationNameLong;
   public $vendorLicenseNumber;
   public $boardingSchoolIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
