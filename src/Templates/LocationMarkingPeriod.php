<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class LocationMarkingPeriod implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $markingPeriodCode;
   public $schoolYearDate;
   public $markingPeriodStartDate;
   public $markingPeriodEndDate;
   public $locationMarkingPeriodDescription;
   public $termCode;
   public $instructionalDays;
   public $instructionalMinutes;
   public $locationTermCategory;
   public $schoolDayMinutes;
   public $daysInSession;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
