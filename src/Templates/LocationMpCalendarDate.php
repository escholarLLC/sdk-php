<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class LocationMpCalendarDate implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $markingPeriodCode;
   public $termCode;
   public $schoolYearDate;
   public $calendarDate;
   public $dayType;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
