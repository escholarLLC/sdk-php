<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class MarkingPeriodCode implements JsonSerializable {

   public $markingPeriodCode;
   public $schoolYearDate;
   public $markingPeriodName;
   public $markingPeriodDescription;
   public $markingPeriodCategory;
   public $markingPeriodSortSequence;
   public $termCode;
   public $termDescription;
   public $termCategory;
   public $termSortSequence;
   public $termMarkingPeriodDescription;
   public $markingPeriodId;
   public $termId;
   public $alternateMarkingPeriodDescription;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
