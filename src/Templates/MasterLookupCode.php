<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class MasterLookupCode implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $lookupName;
   public $lookupCode;
   public $lookupDesc;
   public $alternateLookupDesc;
   public $alternateLookupDesc2;
   public $lookupSortSequence;
   public $lookupNameId;
   public $lookupShortDesc;
   public $standardizedLookupDesc;
   public $standardizedLookupCategory;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
