<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class MeasureCategorySet implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $categorySetCode;
   public $categorySetType;
   public $primaryMeasureType;
   public $countValidationType;
   public $amountValidationType;
   public $percentValidationType;
   public $indicatorValidationType;
   public $category01;
   public $category02;
   public $category03;
   public $category04;
   public $category05;
   public $category06;
   public $category07;
   public $category08;
   public $category09;
   public $category10;
   public $categorySetDescription;
   public $categorySetDateOptionCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
