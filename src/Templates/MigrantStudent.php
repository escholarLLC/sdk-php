<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class MigrantStudent implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $reportingDate;
   public $msixStudentId;
   public $lastQualifyingArrivalDate;
   public $qualifyingMoveFromCity;
   public $qualifyingMoveFromStateCode;
   public $qualifyingMoveFromCountryCode;
   public $qualifyingMoveToCity;
   public $qualifyingMoveToStateCode;
   public $eligibilityExpirationDate;
   public $designatedGraduationHighSchoolNcesId;
   public $migrantCoeStatusIndicator;
   public $lastQualifyingMoveDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
