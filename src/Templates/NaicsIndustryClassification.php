<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class NaicsIndustryClassification implements JsonSerializable {

   public $naicsCode;
   public $naicsEditionYear;
   public $sectorCode;
   public $sectorDescription;
   public $subsectorCode;
   public $subsectorDescription;
   public $industryGroupCode;
   public $industryGroupDescription;
   public $naicsIndustryCode;
   public $naicsIndustryDescription;
   public $nationalIndustryCode;
   public $nationalIndustryDescription;
   public $naicsSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
