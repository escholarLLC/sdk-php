<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class NscCollege implements JsonSerializable {

   public $opeId;
   public $collegeScopeCode;
   public $collegeTypeCode;
   public $collegeName;
   public $collegeStateCode;
   public $requestSchoolYearDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
