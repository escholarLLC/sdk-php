<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class NscStudentCollegeSummary implements JsonSerializable {

   public $districtCode;
   public $studentId;
   public $opeId;
   public $collegeScopeCode;
   public $collegeTypeCode;
   public $requestSchoolYearDate;
   public $collegeEnrollmentStartDate;
   public $collegeEnrollmentEndDate;
   public $collegeEnrollmentStatusCode;
   public $collegeGraduationDate;
   public $collegeDegreeTitle;
   public $collegeDegreeMajor;
   public $collegeEnrollmentClassLevelCode;
   public $collegeEnrollmentMajor;
   public $collegeEnrollmentCipCode;
   public $collegeEnrollmentMajor2;
   public $collegeEnrollmentCip2Code;
   public $collegeDegreeCipCode;
   public $collegeDegreeMajor2;
   public $collegeDegreeCip2Code;
   public $collegeDegreeMajor3;
   public $collegeDegreeCip3Code;
   public $collegeDegreeMajor4;
   public $collegeDegreeCip4Code;
   public $collegeSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
