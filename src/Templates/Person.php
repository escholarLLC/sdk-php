<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class Person implements JsonSerializable {

   public $submittingDistrictCode;
   public $personId;
   public $schoolYearDate;
   public $personDistrictCode;
   public $studentOrStaffId;
   public $localPersonId;
   public $firstName;
   public $lastName;
   public $raceOrEthnicityCode;
   public $genderCode;
   public $personTypeCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
