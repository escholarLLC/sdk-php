<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class PimsSchoolCalendar implements JsonSerializable {

   public $districtCodeOfInstruction;
   public $schoolYearDate;
   public $calendarId;
   public $calendarDescription;
   public $calendarCategory;
   public $calendarGradeLevelDescription;
   public $rotationPatternCode;
   public $calendarProgramsCode;
   public $calendarStartDate;
   public $calendarEndDate;
   public $instructionStartDate;
   public $instructionEndDate;
   public $graduationCeremonyDate;
   public $numberOfScheduledSchoolDays;
   public $instructionalMinutesInStandardDay;
   public $totalDaysInSessionLostDueToStrike;
   public $totalDaysInSessionLostDueToAct80;
   public $totalDaysInSessionLostDueToOther;
   public $totalMakeUpDays;
   public $totInstrMinLostShortenedDaysemergencyWaiver;
   public $act80Group;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
