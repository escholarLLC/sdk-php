<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class PimsStudentCalendarFact implements JsonSerializable {

   public $districtCodeOfInstruction;
   public $schoolYearDate;
   public $calendarId;
   public $studentId;
   public $districtCodeOfResidence;
   public $fundingDistrictCode;
   public $residenceStatusCode;
   public $daysEnrolled;
   public $daysPresent;
   public $percentageOfTimeEnrolledForCalendar;
   public $studentGradeLevel;
   public $homeboundInstructionalMinutes;
   public $sendingCharterSchoolCode;
   public $districtCodeOfSendingCharterSchool;
   public $specialEducationIndicator;
   public $daysAbsentWithoutExcuse;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
