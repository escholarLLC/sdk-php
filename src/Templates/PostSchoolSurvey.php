<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class PostSchoolSurvey implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $studentSnapshotDate;
   public $specialEdOutcomeCode;
   public $postGraduateActivityCode;
   public $specialEdOutcomeDeterminationCode;
   public $postGraduateActivityDeterminationCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
