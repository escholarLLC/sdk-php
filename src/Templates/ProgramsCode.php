<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class ProgramsCode implements JsonSerializable {

   public $districtCode;
   public $programsCode;
   public $schoolYearDate;
   public $programsDesc;
   public $programsShortDesc;
   public $programsType;
   public $stateProgramsCode;
   public $stateProgramsDesc;
   public $programSortSequence;
   public $programAccreditation;
   public $technicalProgram;
   public $programsCategory;
   public $alternateProgramsCategory;
   public $programSponsorCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
