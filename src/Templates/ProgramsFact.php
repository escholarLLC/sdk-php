<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class ProgramsFact implements JsonSerializable {

   public $districtCode;
   public $programLocationCode;
   public $schoolYearDate;
   public $studentId;
   public $programsCode;
   public $programStartDate;
   public $programEndDate;
   public $stateLocationId;
   public $programIntensity;
   public $entryReasonCode1;
   public $entryReasonCode2;
   public $entryReasonCode3;
   public $exitReasonCode1;
   public $exitReasonCode2;
   public $exitReasonCode3;
   public $programComment;
   public $originalPgmStartDate;
   public $pgmParticipationInfoCode;
   public $programFrequency;
   public $programDuration;
   public $programCycle;
   public $programProviderName;
   public $programProviderTypeCode;
   public $programLocationDistrictCode;
   public $programStudentId;
   public $parentalPermissionCode;
   public $programSessionCode;
   public $programEligibilityCode1;
   public $programEligibilityCode2;
   public $programEligibilityCode3;
   public $programEligibilityCode4;
   public $programEligibilityCode5;
   public $programEligibilityCode6;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
