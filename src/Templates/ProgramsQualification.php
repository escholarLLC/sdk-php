<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class ProgramsQualification implements JsonSerializable {

   public $districtCode;
   public $programLocationCode;
   public $schoolYearDate;
   public $studentId;
   public $programsCode;
   public $qualificationDate;
   public $qualificationType;
   public $qualificationInfoCode;
   public $qualificationComment;
   public $programLocationDistrictCode;
   public $qualificationOutcomeCode;
   public $qualificationSource;
   public $qualificationExpirationDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
