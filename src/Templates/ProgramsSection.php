<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class ProgramsSection implements JsonSerializable {

   public $programLocationDistrictCode;
   public $programLocationCode;
   public $programsCode;
   public $schoolYearDate;
   public $programSectionCode;
   public $reportingDate;
   public $roomAssignment;
   public $hoursPerDay;
   public $sectionSize;
   public $maximumSectionSize;
   public $firstStudentEnrollmentDate;
   public $programCurriculumCode;
   public $progressEvaluationToolCode;
   public $developmentalScreeningToolCode;
   public $fundsPaidToProvider;
   public $daysPerWeek;
   public $programScheduleOptionCode;
   public $languageTranslationServicesIndicator;
   public $classGroupName;
   public $programSectionMinimumAge;
   public $programSectionMaximumAge;
   public $servesSpecialNeedsIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
