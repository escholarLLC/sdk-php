<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class ProgramsSectionInstructor implements JsonSerializable {

   public $programLocationDistrictCode;
   public $programLocationCode;
   public $programsCode;
   public $schoolYearDate;
   public $programSectionCode;
   public $reportingDate;
   public $instructorDistrictCode;
   public $instructorId;
   public $instructorStartDate;
   public $instructorEndDate;
   public $instructorTypeCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
