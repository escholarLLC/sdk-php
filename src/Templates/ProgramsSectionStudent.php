<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class ProgramsSectionStudent implements JsonSerializable {

   public $programLocationDistrictCode;
   public $programLocationCode;
   public $programsCode;
   public $schoolYearDate;
   public $programSectionCode;
   public $reportingDate;
   public $studentDistrictCode;
   public $studentId;
   public $studentEntryDate;
   public $studentExitDate;
   public $studentExitReasonCode;
   public $studentProgramCost;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
