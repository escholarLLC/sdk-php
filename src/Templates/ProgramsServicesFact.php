<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class ProgramsServicesFact implements JsonSerializable {

   public $districtCode;
   public $serviceLocationCode;
   public $schoolYearDate;
   public $studentId;
   public $serviceCode;
   public $serviceStartDate;
   public $serviceEndDate;
   public $serviceFrequency;
   public $serviceDuration;
   public $serviceCycleCode;
   public $serviceLocationDistrictCode;
   public $serviceProviderName;
   public $serviceProviderTypeCode;
   public $servicePrimaryFundSourceCode;
   public $serviceParticipationInfoCode;
   public $serviceSettingCode;
   public $totalServiceUnits;
   public $primaryStaffDistrictCode;
   public $primaryStaffId;
   public $serviceSettingTypeCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
