<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class ReasonsCode implements JsonSerializable {

   public $districtCode;
   public $reasonType;
   public $reasonCode;
   public $schoolYearDate;
   public $reasonDesc;
   public $stateReasonCode;
   public $stateReasonDesc;
   public $reasonSortSequence;
   public $reasonCategory;
   public $reasonShortDesc;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
