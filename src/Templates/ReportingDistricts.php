<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class ReportingDistricts implements JsonSerializable {

   public $districtCode;
   public $reportingDistrictCode;
   public $reportingDistrictName;
   public $reportingDistrictType;
   public $countyName;
   public $stateDistrictId;
   public $fullContactName;
   public $contactPositionTitle;
   public $locationAddress1;
   public $locationAddress2;
   public $locationAddress3;
   public $locationCity;
   public $locationStateCode;
   public $locationZipCode;
   public $locationZipCode4;
   public $contactFirstName;
   public $contactLastName;
   public $contactMiddleName;
   public $contactNameSuffix;
   public $contactEmailAddress;
   public $contactPhoneNumber;
   public $stateCountyCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
