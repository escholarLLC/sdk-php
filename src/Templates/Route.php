<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class Route implements JsonSerializable {

   public $districtCode;
   public $routeId;
   public $schoolYearDate;
   public $routeShortDescription;
   public $routeLongDescription;
   public $routeCategoryCode;
   public $routeTypeCode;
   public $routeSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
