<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SchoolEnrollment implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $studentId;
   public $activityDate;
   public $enrollmentDate;
   public $enrollmentCode;
   public $enrollmentComment;
   public $enrollmentGradeLevel;
   public $residenceStatusCode;
   public $enrollChangeCode;
   public $districtCodeOfResidence;
   public $enrolledAtSchoolYearStartIndicator;
   public $schoolChoiceTransferIndicator;
   public $classOfYearCode;
   public $employedIndicator;
   public $primaryEnrollmentLocationIndicator;
   public $promotionRetentionReasonCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
