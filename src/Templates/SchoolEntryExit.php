<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SchoolEntryExit implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $studentId;
   public $schoolEntryDate;
   public $schoolEntryTypeCode;
   public $schoolEntryComment;
   public $enrollmentGradeLevel;
   public $residenceStatusCode;
   public $enrollChangeCode;
   public $schoolExitDate;
   public $schoolExitTypeCode;
   public $schoolExitComment;
   public $districtCodeOfResidence;
   public $enrolledAtSchoolYearStartIndicator;
   public $locationCodeOfResidence;
   public $schoolChoiceTransferIndicator;
   public $gradeLevelProgramTypeCode;
   public $classOfYearCode;
   public $employedIndicator;
   public $displacedStudentIndicator;
   public $primaryEnrollmentLocationIndicator;
   public $promotionRetentionReasonCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
