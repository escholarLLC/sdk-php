<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SpecialEducationEventTypes implements JsonSerializable {

   public $districtCode;
   public $eventTypeCode;
   public $schoolYearDate;
   public $eventTypeDesc;
   public $eventTypeCategory;
   public $stateEventTypeCode;
   public $stateEventTypeDesc;
   public $eventTypeSortSequence;
   public $eventChainCategory;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
