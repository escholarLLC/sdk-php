<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SpecialEducationEvents implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $studentId;
   public $eventTypeCode;
   public $eventDate;
   public $eventReasonCode1;
   public $eventReasonCode2;
   public $eventReasonCode3;
   public $eventSourceCode;
   public $eventStatusCode;
   public $eventOutcomeCode;
   public $meetingDate;
   public $meetingTypeCode;
   public $effectiveDate;
   public $organizationCode;
   public $organizationName;
   public $organizationCluster;
   public $complianceDate;
   public $nonComplianceReason;
   public $challengeType;
   public $challengeTypeDisorder1;
   public $challengeTypeDisorder2;
   public $challengeTypeDisorder3;
   public $previousChallengeType;
   public $integratedPercentage;
   public $specialEdPercentage;
   public $studentPresent;
   public $parentPresent;
   public $eventEndDate;
   public $initialEventTypeCode;
   public $initialEventDate;
   public $numberOfDays;
   public $meetingLocationTypeCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
