<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SpecialEducationServicesFact implements JsonSerializable {

   public $districtCode;
   public $serviceLocationCode;
   public $schoolYearDate;
   public $studentId;
   public $serviceCode;
   public $placementType;
   public $complianceDate;
   public $serviceStartDate;
   public $serviceEndDate;
   public $serviceIdentifier;
   public $serviceSize;
   public $serviceFrequency;
   public $serviceDuration;
   public $serviceCycle;
   public $integratedServiceStatus;
   public $integratedLocationStatus;
   public $sectionNumber;
   public $nonComplianceReason;
   public $placementArrangedDate;
   public $placementStatus;
   public $serviceProviderName;
   public $serviceProviderTypeCode;
   public $serviceLocationDistrictCode;
   public $serviceParticipationInfoCode;
   public $serviceSettingCode;
   public $totalServiceUnits;
   public $primaryStaffDistrictCode;
   public $primaryStaffId;
   public $serviceSettingTypeCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
