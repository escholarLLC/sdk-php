<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SpecialEducationStudent implements JsonSerializable {

   public $districtCode;
   public $primarySpecialEdLocationCode;
   public $schoolYearDate;
   public $studentId;
   public $lastNameShort;
   public $firstNameShort;
   public $middleInitial;
   public $currentGradeLevel;
   public $specialEducationTeacherName;
   public $primaryDisabilityCode;
   public $secondaryDisabilityCode;
   public $lastIepDate;
   public $lastEvaluationDate;
   public $serviceLevelCode;
   public $relatedServices;
   public $supportTypeCode;
   public $specialEducationTeacherId;
   public $annualReviewDate;
   public $triennialReviewDate;
   public $expectedDiplomaType;
   public $secondLanguageExempt;
   public $alternateAssessment;
   public $instructionLanguage;
   public $communicationMode;
   public $brailleInstruction;
   public $specialTransportation;
   public $extendedSchoolYear;
   public $homeSchooled;
   public $programsCode;
   public $primaryServiceCode;
   public $primaryPlacementType;
   public $specialEdEntryDate;
   public $specialEdExitDate;
   public $notApplicable;
   public $iepCompliance;
   public $fundingPrimaryDisabilityCode;
   public $disabilityPrimaryCauseCode;
   public $livingSettingCode;
   public $hearingImpairmentLevelCode;
   public $visionImpairmentLevelCode;
   public $tertiaryDisabilityCode;
   public $quaternaryDisabilityCode;
   public $primarySettingCode;
   public $transitionIepStatusCode;
   public $primaryServiceProviderLocationCode;
   public $schoolAgedIndicator;
   public $surrogateAppointedCode;
   public $programIntegratedPercentage;
   public $programSpecialEdPercentage;
   public $primaryEntryReasonCode;
   public $primaryExitReasonCode;
   public $lepStatusIndicatorAtExit;
   public $servicePlanTypeCode;
   public $specialEdStudentId;
   public $ageGroup;
   public $mentalHealthServicesEligibilityIndicator;
   public $specialEdLocationDistrictCode;
   public $primaryAreaOfExceptionality;
   public $studentSnapshotDate;
   public $enrolledAtSchoolYearStartIndicator;
   public $multipleDisabilitiesDesignationIndicator;
   public $medicallyFragileIndicator;
   public $childCountFederalProgramTypeCode;
   public $primaryPlacementReasonCode;
   public $districtCodeOfResidence;
   public $locationCodeOfResidence;
   public $childCountDistrictCode;
   public $childCountLocationCode;
   public $primaryServiceProviderName;
   public $primaryServiceBuildingName;
   public $levelOfIntegrationCode;
   public $transitionIepPaidWorkIndicator;
   public $transitionIepJobSupportIndicator;
   public $transitionIepCareerExplorationIndicator;
   public $transitionIepModifiedPaidWorkIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
