<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffAssignedCohortGroup implements JsonSerializable {

   public $staffDistrictCode;
   public $staffId;
   public $cohortGroupDistrictCode;
   public $cohortGroupLocationCode;
   public $cohortGroupId;
   public $schoolYearDate;
   public $reportingDate;
   public $staffAssignedStartDate;
   public $staffAssignedEndDate;
   public $studentRecordAccessIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
