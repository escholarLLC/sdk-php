<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffAssignment implements JsonSerializable {

   public $districtCode;
   public $staffId;
   public $assignmentCode;
   public $assignmentLocationCode;
   public $schoolYearDate;
   public $assignmentDate;
   public $completionDate;
   public $assignmentJobCode;
   public $assignmentField;
   public $assignmentRoom;
   public $assignmentSite;
   public $assignmentProgramsCode;
   public $assignmentPrimaryFundSourceCode;
   public $assignmentQualificationStatusCode;
   public $subjectCode;
   public $percentTimeAssigned;
   public $assignmentFieldStatus;
   public $assignmentGradeLevel;
   public $controllingDistrictCode;
   public $assignmentSecondaryFundSourceCode;
   public $assignmentTertiaryFundSourceCode;
   public $assignmentQuaternaryFundSourceCode;
   public $assignmentTitle;
   public $primaryAssignmentIndicator;
   public $assignmentOrder;
   public $assignmentStudentCount;
   public $yearsExperienceInAssignment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
