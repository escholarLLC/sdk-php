<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffAssignmentCode implements JsonSerializable {

   public $districtCode;
   public $assignmentCode;
   public $assignmentDescription;
   public $assignmentCategory;
   public $assignmentSortSequence;
   public $stateAssignmentCode;
   public $stateAssignmentDescription;
   public $schoolYearDate;
   public $assignmentShortDescription;
   public $alternateAssignmentCategory;
   public $assignmentRoleTypeCode;
   public $assignmentRoleTypeDescription;
   public $assignmentRoleSubtypeCode;
   public $assignmentRoleSubtypeDescription;
   public $assignmentLocationTypeCode;
   public $assignmentLocationTypeDescription;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
