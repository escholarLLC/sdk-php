<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffAttendance implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $staffId;
   public $staffAttendanceCode;
   public $attendancePeriodStartDate;
   public $attendancePeriodEndDate;
   public $timeEarned;
   public $timeUsed;
   public $timeAccrued;
   public $substituteTeacherId;
   public $staffAttendanceCodeLong;
   public $unitOfMeasureCode;
   public $staffAttendanceComment;
   public $staffDistrictCode;
   public $substituteTeacherDistrictCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
