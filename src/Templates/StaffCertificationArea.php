<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffCertificationArea implements JsonSerializable {

   public $districtCode;
   public $certificationAreaCode;
   public $certificationAreaDesc;
   public $subjectAreaCode;
   public $certificationAreaCategory;
   public $certificationAreaSortSequence;
   public $requiredCertificationAreaInd;
   public $schoolYearDate;
   public $certificationAreaShortDesc;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
