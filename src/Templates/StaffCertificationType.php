<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffCertificationType implements JsonSerializable {

   public $districtCode;
   public $certificationTypeCode;
   public $certificationTypeDesc;
   public $certificationTypeCategory;
   public $nationalCertificationIndicator;
   public $stateCertificationIndicator;
   public $certificationTypeSortSequence;
   public $requiredCertificationTypeInd;
   public $schoolYearDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
