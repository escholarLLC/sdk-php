<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffCertifications implements JsonSerializable {

   public $districtCode;
   public $staffId;
   public $certificationTypeCode;
   public $certificationAreaCode;
   public $certificationEffectiveDate;
   public $expirationDate;
   public $requiredByDate;
   public $certificationStatusCode;
   public $certificateNumber;
   public $originalCertificationDate;
   public $permanentCertificationIndicator;
   public $certificationLevelCode;
   public $certificationBasisCode;
   public $teachingCredentialTypeCode;
   public $sponsorTypeCode;
   public $certificateName;
   public $certificateIssuerName;
   public $certificateIssuerStateCode;
   public $certificateIssueDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
