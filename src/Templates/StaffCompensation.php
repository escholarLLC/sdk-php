<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffCompensation implements JsonSerializable {

   public $districtCode;
   public $reportingDate;
   public $staffId;
   public $compensationType;
   public $compensationCategory;
   public $employeeType;
   public $compensationAmount;
   public $compensationPercent;
   public $locationCode;
   public $staffSnapshotDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
