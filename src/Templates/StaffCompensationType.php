<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffCompensationType implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $compensationType;
   public $compensationCategory;
   public $employeeType;
   public $compensationDesc;
   public $compensationTypeSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
