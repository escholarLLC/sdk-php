<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffDegreeType implements JsonSerializable {

   public $districtCode;
   public $staffId;
   public $degreeTypeDifferentiatorNumber;
   public $schoolYearDate;
   public $degreeTypeCode;
   public $institutionCode;
   public $localInstitutionName;
   public $degreeAwardedYear;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
