<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffDevelopmentActivity implements JsonSerializable {

   public $districtCode;
   public $activityCode;
   public $activityName;
   public $activityDescription;
   public $activitySubjectAreaCode;
   public $activityProviderName;
   public $activityCategory;
   public $stateActivityCode;
   public $stateActivityDescription;
   public $activitySortSequence;
   public $schoolYearDate;
   public $activityCost;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
