<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffDevelopmentFact implements JsonSerializable {

   public $districtCode;
   public $staffId;
   public $activityCode;
   public $activityStartDate;
   public $activityWithdrawalDate;
   public $activityCompletionDate;
   public $participationLevelCode;
   public $purposeCode;
   public $relevanceCode;
   public $mediumOfInstructionCode;
   public $activityFormatCode;
   public $creditTypeCode;
   public $staffCreditsEarned;
   public $activityHours;
   public $certificateExpirationDate;
   public $tuitionFundedIndicator;
   public $activityLanguageCode;
   public $activityObjective;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
