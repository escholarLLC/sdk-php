<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffEvaluationRating implements JsonSerializable {

   public $staffDistrictCode;
   public $staffId;
   public $evaluationCriteriaCode;
   public $schoolYearDate;
   public $reportingDate;
   public $evaluationReviewDate;
   public $evaluationCriteriaRatingCode;
   public $evaluationCriteriaRatingPoints;
   public $evaluationPeriodStartDate;
   public $evaluationPeriodEndDate;
   public $evaluatorStaffId;
   public $evaluatorDistrictCode;
   public $evaluatorComment;
   public $alternateStaffId;
   public $evaluationGroupCode;
   public $courseDistrictCode;
   public $courseLocationCode;
   public $courseCode;
   public $supplementaryCourseDifferentiator;
   public $sectionCode;
   public $evaluationReportingDistrictCode;
   public $evaluationReportingLocationCode;
   public $evaluationChangeReasonCode;
   public $evaluationChangeComment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
