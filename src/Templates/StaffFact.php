<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffFact implements JsonSerializable {

   public $districtCode;
   public $staffId;
   public $schoolYearDate;
   public $categorySetCode;
   public $primaryMeasureType;
   public $reportingDate;
   public $count;
   public $amount;
   public $percent;
   public $indicator;
   public $startDate;
   public $endDate;
   public $comment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
