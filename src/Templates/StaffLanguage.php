<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffLanguage implements JsonSerializable {

   public $districtCode;
   public $staffId;
   public $schoolYearDate;
   public $languageCode;
   public $languageUse;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
