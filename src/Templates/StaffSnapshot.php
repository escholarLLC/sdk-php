<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffSnapshot implements JsonSerializable {

   public $districtCode;
   public $staffId;
   public $socialSecurityNumber;
   public $lastNameShort;
   public $firstNameShort;
   public $middleInitial;
   public $jobClassCode;
   public $positionTitle;
   public $extraJobClassCode1;
   public $extraJobClassCode2;
   public $extraJobClassCode3;
   public $extraJobClassCode4;
   public $extraJobClassCode5;
   public $primaryLocationCode;
   public $division;
   public $department;
   public $staffEmploymentTypeCode;
   public $union;
   public $supervisor;
   public $genderCode;
   public $raceOrEthnicityCode;
   public $homePhone;
   public $unlistedPhoneNumberStatus;
   public $address1;
   public $address2;
   public $city;
   public $stateCode;
   public $fullZipCode;
   public $rankperformanceTier;
   public $medicalExam;
   public $substanceTest;
   public $originalEmploymentStartDate;
   public $positionTenureDate;
   public $currentServiceDate;
   public $exitDate;
   public $staffCitizenshipCode;
   public $felony;
   public $yearsExperience;
   public $birthDate;
   public $activeinactiveIndicator;
   public $lastStatusDate;
   public $yearsExperienceInDistrict;
   public $jobClassCodeLong;
   public $highestDegreeEarned;
   public $yearsOfHigherEducation;
   public $militaryDutyStatus;
   public $staffQualificationStatusCode;
   public $jobClassDescription;
   public $snapshotDate;
   public $payStepLevel;
   public $annualSalary;
   public $contractWorkDays;
   public $employmentSeparationReasonCode;
   public $fullStaffName;
   public $itinerantTeacher;
   public $alternateStaffId;
   public $alternateAnnualSalary;
   public $alternateContractWorkDays;
   public $employmentBasis;
   public $alternateEmploymentBasis;
   public $benefitsValue;
   public $localContract;
   public $contractingOrganization;
   public $firstNameLong;
   public $lastNameLong;
   public $nameSuffix;
   public $payStepLevelAlpha;
   public $hispanicEthnicityIndicator;
   public $raceOrEthnicitySubgroupCode;
   public $highestDegreeInstitutionCode;
   public $baccalaureateDegreeInstitutionCode;
   public $address3;
   public $baseZipCode;
   public $zipCode4;
   public $workEmailAddress;
   public $middleName;
   public $race2Code;
   public $race3Code;
   public $race4Code;
   public $race5Code;
   public $postsecondarySubjectArea;
   public $employmentEligibilityVerification;
   public $namePrefix;
   public $workPhone;
   public $cellPhone;
   public $mailingAddress1;
   public $mailingAddress2;
   public $mailingAddress3;
   public $mailingAddressCity;
   public $mailingAddressStateCode;
   public $mailingAddressBaseZipCode;
   public $mailingAddressZipCode4;
   public $employmentStatusCode;
   public $authorizedToCarryWeaponIndicator;
   public $staffFirstNameAlias;
   public $staffPreviousLastName;
   public $psStateStudentId;
   public $personalEmailAddress;
   public $employmentSeparationTypeCode;
   public $previousStaffId;
   public $yearsOfTeachingExperience;
   public $personalInformationVerificationCode;
   public $positionHireDate;
   public $secondPositionTitle;
   public $secondPositionHireDate;
   public $secondPositionTenureDate;
   public $staffEducationLevelCode;
   public $employmentOfferDate;
   public $annualContractWorkMonths;
   public $professionalDevelopmentIndicator;
   public $certificationExemptionCode;
   public $countryCode;
   public $mailingAddressCountryCode;
   public $staffApprovalStatusCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
