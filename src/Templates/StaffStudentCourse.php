<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffStudentCourse implements JsonSerializable {

   public $staffDistrictCode;
   public $staffId;
   public $studentDistrictCode;
   public $studentId;
   public $schoolYearDate;
   public $courseDistrictCode;
   public $courseLocationCode;
   public $courseCode;
   public $supplementaryCourseDifferentiator;
   public $sectionCode;
   public $reportingDate;
   public $relationshipStartDate;
   public $relationshipEndDate;
   public $potentialStudentInstructionalTime;
   public $actualStudentInstructionalTime;
   public $instructionalResponsibilityWeight;
   public $excludeFromEvaluationIndicator;
   public $totalPlannedClassTime;
   public $termCode;
   public $instructionalRelationshipWeight;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
