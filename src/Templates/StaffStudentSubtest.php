<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffStudentSubtest implements JsonSerializable {

   public $staffDistrictCode;
   public $staffId;
   public $studentDistrictCode;
   public $studentId;
   public $schoolYearDate;
   public $assessmentDistrictCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $instructionalResponsibilityWeight;
   public $instructionalRelationshipWeight;
   public $relationshipLocationCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
