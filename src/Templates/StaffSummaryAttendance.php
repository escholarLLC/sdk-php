<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffSummaryAttendance implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $attendancePeriodStartDate;
   public $attendancePeriodEndDate;
   public $timeEarned;
   public $timeUsed;
   public $timeAccrued;
   public $staffAttendanceCodeLong;
   public $unitOfMeasureCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
