<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StaffTenureSnapshot implements JsonSerializable {

   public $districtCode;
   public $staffId;
   public $alternateStaffId;
   public $tenureAreaCode;
   public $schoolYearDate;
   public $tenureStatusCode;
   public $tenureStatusEffectiveDate;
   public $originalProbationaryPeriodEndDate;
   public $probationaryPeriodEndDate;
   public $probationaryPeriodExtendedIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
