<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StateAgency implements JsonSerializable {

   public $districtCode;
   public $stateAgencyCode;
   public $stateAgencyName;
   public $stateAgencyNumber;
   public $stateAgencyWebSiteUrl;
   public $stateCode;
   public $ansiStateCode;
   public $lastStatusDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
