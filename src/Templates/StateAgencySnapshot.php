<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StateAgencySnapshot implements JsonSerializable {

   public $districtCode;
   public $stateAgencyCode;
   public $reportingDate;
   public $dunsNumber;
   public $ansiStateCode;
   public $locationLatitude;
   public $locationLongitude;
   public $locationAddress1;
   public $locationAddress2;
   public $locationAddress3;
   public $locationCity;
   public $locationStateCode;
   public $locationZipCode;
   public $locationZipCode4;
   public $mailingAddress1;
   public $mailingAddress2;
   public $mailingAddress3;
   public $mailingCity;
   public $mailingStateCode;
   public $mailingZipCode;
   public $mailingZipCode4;
   public $agencyPhoneNumber;
   public $chiefStateSchoolOfficerFirstName;
   public $chiefStateSchoolOfficerLastName;
   public $chiefStateSchoolOfficerPositionTitle;
   public $chiefStateSchoolOfficerPhoneNumber;
   public $chiefStateSchoolOfficerEmailAddress;
   public $stateAypStatus;
   public $lastStatusDate;
   public $stateAgencyName;
   public $chiefStateSchoolOfficerNamePrefix;
   public $amaoLepProficiencyStatus;
   public $amaoLepProgressStatus;
   public $improvementStatus;
   public $aypAppealsCompletionDate;
   public $titleIiiFundsReceivedDate;
   public $titleIiiFundsAvailableDate;
   public $stateAgencyLegalName;
   public $stateAgencyWebSiteUrl;
   public $stateAgencyOperationalStatus;
   public $averageNumberOfDaysForTitleIiiSubgrants;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
