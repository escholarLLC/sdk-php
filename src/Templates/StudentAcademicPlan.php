<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentAcademicPlan implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $academicPlanCode;
   public $academicPlanType;
   public $locationCode;
   public $reportingDate;
   public $individualPlanIndicator;
   public $totalCreditsRequired;
   public $minimumGpaRequired;
   public $studentPlanUpdateDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
