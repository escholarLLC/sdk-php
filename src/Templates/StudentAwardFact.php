<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentAwardFact implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $awardCode;
   public $awardType;
   public $awardDate;
   public $awardEarnedThroughAppealIndicator;
   public $markingPeriodCode;
   public $termCode;
   public $awardExpirationDate;
   public $awardIssuerName;
   public $awardIssuerWebAddress;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
