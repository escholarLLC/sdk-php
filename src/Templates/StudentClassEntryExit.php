<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentClassEntryExit implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $courseCode;
   public $schoolYearDate;
   public $supplementaryCourseDifferentiator;
   public $sectionCode;
   public $studentId;
   public $classEntryDate;
   public $classEntryTypeCode;
   public $classEntryComment;
   public $classExitDate;
   public $classExitTypeCode;
   public $classExitComment;
   public $classPeriod;
   public $courseDeliveryModelCode;
   public $courseContentCode;
   public $courseInclusionCode;
   public $specialProgramCode;
   public $alternateCreditCourseCode;
   public $courseInstructorSnapshotDate;
   public $homeroomIndicator;
   public $studentGradeLevelCodeWhenTaken;
   public $excludeFromEvaluationIndicator;
   public $termCode;
   public $markingPeriodCode;
   public $dualCreditIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
