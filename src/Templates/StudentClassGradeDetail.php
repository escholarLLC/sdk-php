<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentClassGradeDetail implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $courseCode;
   public $schoolYearDate;
   public $supplementaryCourseDifferentiator;
   public $sectionCode;
   public $studentId;
   public $gradeDetailCode;
   public $reportingDate;
   public $markingPeriodCode;
   public $displayGrade;
   public $alphaGrade;
   public $numericGrade;
   public $classDetailOutcomeCode;
   public $gpaImpactCode;
   public $evaluator1StaffId;
   public $studentClassGradeDetailComment;
   public $courseInstructorSnapshotDate;
   public $evaluator1ControllingDistrictCode;
   public $termCode;
   public $stateAssessmentIncludedIndicator;
   public $creditsAttempted;
   public $creditsEarned;
   public $studentClassCreditTypeCode;
   public $dualCreditCode;
   public $cumulativeClockTime;
   public $evaluator2StaffId;
   public $evaluator3StaffId;
   public $evaluator2ControllingDistrictCode;
   public $evaluator3ControllingDistrictCode;
   public $instructionTypeCode;
   public $instructionDeliveryMethodCode;
   public $instructionMediumTypeCode;
   public $courseDeliveryModelCode;
   public $courseCompletionIndicator;
   public $postsecondaryCreditUnits;
   public $creditRecoveryCode;
   public $studentGradeLevelCodeWhenTaken;
   public $gradeStandardPerformanceLevelCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
