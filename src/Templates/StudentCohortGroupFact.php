<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentCohortGroupFact implements JsonSerializable {

   public $studentDistrictCode;
   public $studentId;
   public $cohortGroupDistrictCode;
   public $cohortGroupLocationCode;
   public $cohortGroupId;
   public $schoolYearDate;
   public $reportingDate;
   public $studentCohortGroupStartDate;
   public $studentCohortGroupEndDate;
   public $studentWeeklyHours;
   public $studentYearlyWeeks;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
