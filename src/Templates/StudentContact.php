<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentContact implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $contactId;
   public $contactFirstName;
   public $contactMiddleName;
   public $contactLastName;
   public $contactGenderCode;
   public $contactRelationshipCode;
   public $contactAuthorizationCode;
   public $contactPhone1;
   public $contactPhone1TypeCode;
   public $contactPhone2;
   public $contactPhone2TypeCode;
   public $contactPhone3;
   public $contactPhone3TypeCode;
   public $contactEmailAddress;
   public $contactOralLanguageCode;
   public $contactWrittenLanguageCode;
   public $contactSequenceNumber;
   public $activeIndicator;
   public $contactMilitaryStatusCode;
   public $contactEmploymentTypeCode;
   public $contactNamePrefix;
   public $contactNameSuffix;
   public $contactHomeAddress1;
   public $contactHomeAddress2;
   public $contactHomeAddress3;
   public $contactHomeCity;
   public $contactHomeStateCode;
   public $contactHomeBaseZipCode;
   public $contactHomeZipCode4;
   public $contactHomeStateCountyCode;
   public $contactMailingAddress1;
   public $contactMailingAddress2;
   public $contactMailingAddress3;
   public $contactMailingCity;
   public $contactMailingStateCode;
   public $contactMailingBaseZipCode;
   public $contactMailingZipCode4;
   public $contactMailingStateCountyCode;
   public $militaryServiceBranch;
   public $militaryInstallation;
   public $militaryUnit;
   public $militaryPayGrade;
   public $militaryActiveDutyIndicator;
   public $nationalGuardIndicator;
   public $militaryReserveIndicator;
   public $nationalGuardreserveActivatedIndicator;
   public $civilServiceEmployeeIndicator;
   public $foreignMilitaryIndicator;
   public $retiredMilitaryIndicator;
   public $militaryVeteranIndicator;
   public $disabledVeteranIndicator;
   public $deceasedIndicator;
   public $killedInActionIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
