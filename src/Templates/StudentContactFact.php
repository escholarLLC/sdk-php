<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentContactFact implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $contactId;
   public $reportingDate;
   public $contactRelationshipCode;
   public $contactAuthorizationCode;
   public $contactSequenceNumber;
   public $activeIndicator;
   public $primaryContactIndicator;
   public $studentLivesWithContactIndicator;
   public $emergencyContactIndicator;
   public $contactRestrictionsDescription;
   public $custodialParentOrGuardianIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
