<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentCourseAttendance implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $courseCode;
   public $sectionCode;
   public $requestSequence;
   public $studentId;
   public $attendanceDate;
   public $attendanceCode;
   public $supplementaryCourseDifferentiator;
   public $attendanceCodeLong;
   public $courseCodeLong;
   public $sectionCodeLong;
   public $courseInstructorSnapshotDate;
   public $attendanceComment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
