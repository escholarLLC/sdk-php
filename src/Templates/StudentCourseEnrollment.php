<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentCourseEnrollment implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $enrollmentPeriodNumber;
   public $effectiveDate;
   public $enrollmentCode;
   public $studentId;
   public $courseCode;
   public $sectionCode;
   public $requestSequence;
   public $classPeriod;
   public $enrollmentComment;
   public $supplementaryCourseDifferentiator;
   public $courseCodeLong;
   public $courseDeliveryModelCode;
   public $courseContentCode;
   public $courseInclusionCode;
   public $specialProgramCode;
   public $alternateCreditCourseCode;
   public $sectionCodeLong;
   public $courseInstructorSnapshotDate;
   public $homeroomIndicator;
   public $gpaImpactCode;
   public $excludeFromEvaluationIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
