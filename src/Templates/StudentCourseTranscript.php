<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentCourseTranscript implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $markingPeriodCode;
   public $termCode;
   public $markingPeriodSchoolYearDate;
   public $creditGpaCode;
   public $catalogCourseCode;
   public $reportingDate;
   public $classDetailOutcomeCode;
   public $creditsAttempted;
   public $creditsEarned;
   public $methodCreditEarnedCode;
   public $alphaGrade;
   public $numericGrade;
   public $gpaImpactCode;
   public $studentGradeLevelCodeWhenTaken;
   public $studentCourseTranscriptComment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
