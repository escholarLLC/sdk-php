<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentCreditGpa implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $creditGpaCode;
   public $reportingDate;
   public $markingPeriodCode;
   public $markingPeriodCreditsEarned;
   public $markingPeriodGpa;
   public $cumulativeCreditsEarned;
   public $cumulativeGpa;
   public $termCode;
   public $markingPeriodGradePoints;
   public $cumulativeGradePoints;
   public $markingPeriodLocationCode;
   public $studentCreditGpaComment;
   public $studentGpaRangeMinimum;
   public $studentGpaRangeMaximum;
   public $markingPeriodCreditsAttempted;
   public $cumulativeCreditsAttempted;
   public $projectedGraduationDate;
   public $classRank;
   public $classSize;
   public $percentageClassRank;
   public $classRankDate;
   public $markingPeriodSchoolYearDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
