<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentDailyAttendance implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $studentId;
   public $attendanceDate;
   public $attendanceCode;
   public $attendanceComment;
   public $absenceDuration;
   public $attendanceCodeLong;
   public $schoolYearDate;
   public $markingPeriodCode;
   public $termCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
