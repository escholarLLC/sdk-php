<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentDailyEnrollment implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $studentId;
   public $enrollmentIndicatorDate;
   public $enrollmentIndicator;
   public $studentGradeLevel;
   public $enrollmentComment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
