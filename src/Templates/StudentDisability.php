<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentDisability implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $disabilityCode;
   public $reportingDate;
   public $disabilityDiagnosisDescription;
   public $disabilityOrder;
   public $disabilityDeterminationSourceTypeCode;
   public $disabilityConditionStatus;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
