<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentEducationBarrier implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $reportingDate;
   public $educationBarrierCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
