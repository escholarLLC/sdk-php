<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentFact implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $reportingDate;
   public $categorySetCode;
   public $primaryMeasureType;
   public $count;
   public $amount;
   public $percent;
   public $indicator;
   public $startDate;
   public $endDate;
   public $comment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
