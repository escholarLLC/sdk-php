<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentGrades implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $courseCode;
   public $sectionCode;
   public $requestSequence;
   public $evaluator1StaffId;
   public $studentId;
   public $markingPeriodNumber;
   public $numericGrade;
   public $alphaGrade;
   public $passfail;
   public $absences;
   public $excusedAbsences;
   public $unexcusedAbsences;
   public $tardies;
   public $excusedTardies;
   public $unexcusedTardies;
   public $gpaImpactCode;
   public $studentGradesComment;
   public $gradeTypeCode;
   public $supplementaryCourseDifferentiator;
   public $creditsAttempted;
   public $creditsEarned;
   public $gradePointsAddOn;
   public $courseCodeLong;
   public $sectionCodeLong;
   public $conductGrade;
   public $courseInstructorSnapshotDate;
   public $evaluator2StaffId;
   public $evaluator3StaffId;
   public $dualCreditCode;
   public $evaluator1ControllingDistrictCode;
   public $evaluator2ControllingDistrictCode;
   public $evaluator3ControllingDistrictCode;
   public $studentClassCreditTypeCode;
   public $cumulativeClockTime;
   public $stateAssessmentIncludedIndicator;
   public $instructionTypeCode;
   public $instructionDeliveryMethodCode;
   public $instructionMediumTypeCode;
   public $courseDeliveryModelCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
