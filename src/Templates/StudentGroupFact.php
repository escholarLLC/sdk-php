<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentGroupFact implements JsonSerializable {

   public $districtCode;
   public $groupCode;
   public $schoolYearDate;
   public $studentId;
   public $groupStatus;
   public $groupEntryDate;
   public $groupEndDate;
   public $groupComment;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
