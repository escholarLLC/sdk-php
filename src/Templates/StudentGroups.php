<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentGroups implements JsonSerializable {

   public $districtCode;
   public $groupCode;
   public $groupName;
   public $groupDescription;
   public $establishmentDate;
   public $discontinueDate;
   public $groupCategory;
   public $groupBudget;
   public $groupSortSequence;
   public $groupScope;
   public $alternateGroupCategory;
   public $groupGenderCompositionCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
