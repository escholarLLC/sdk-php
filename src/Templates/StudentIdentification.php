<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentIdentification implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $informationDate;
   public $socialSecurityNumber;
   public $lastNameLong;
   public $firstNameLong;
   public $middleName;
   public $nameSuffix;
   public $nameAlias;
   public $currentGradeLevel;
   public $locationCode;
   public $raceOrEthnicityCode;
   public $genderCode;
   public $birthDate;
   public $cityOfBirth;
   public $stateOfBirth;
   public $countryOfBirth;
   public $stateStudentId;
   public $stateStudentIdAssignmentDate;
   public $parentName1;
   public $parentName2;
   public $guardianNameLong;
   public $guardianRelationship;
   public $inoculationDate;
   public $grade09EntryDate;
   public $stateLocationId;
   public $languageCode;
   public $address1;
   public $address2;
   public $city;
   public $state;
   public $zipCode;
   public $homePhone;
   public $idComment;
   public $address3;
   public $districtCodeOfResidence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
