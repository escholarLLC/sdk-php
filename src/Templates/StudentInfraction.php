<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentInfraction implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $studentId;
   public $infractionDate;
   public $infractionNumberCount;
   public $infractionCode;
   public $staffId;
   public $courseCode;
   public $sectionCode;
   public $classPeriod;
   public $zone;
   public $administratorId;
   public $eventIdentifier;
   public $infractionComment;
   public $supplementaryCourseDifferentiator;
   public $primaryInfraction;
   public $caseNumber;
   public $weaponCode;
   public $victim1Type;
   public $victim2Type;
   public $victim3Type;
   public $securityTransactionType;
   public $courseCodeLong;
   public $incidentTimeFrame;
   public $criminalChargeCode;
   public $sectionCodeLong;
   public $infractionCost;
   public $drugRelatedIndicator;
   public $alcoholRelatedIndicator;
   public $gangRelatedIndicator;
   public $hateRelatedIndicator;
   public $seriousBodilyInjuryIndicator;
   public $lleNotifiedIndicator;
   public $arrestedCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
