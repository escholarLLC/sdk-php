<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentInfractionResponse implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $studentId;
   public $responseDate;
   public $infractionNumberCount;
   public $responseCode;
   public $administratorId;
   public $dispositionStartDate;
   public $dispositionEndDate;
   public $eventIdentifier;
   public $responseComment;
   public $responseDuration;
   public $caseNumber;
   public $behaviorInterventionPlan;
   public $summerSchool;
   public $specialEducationIndicator;
   public $regularEducationIndicator;
   public $interventionAssistanceTeam;
   public $testingReferral;
   public $adaStatusIndicator;
   public $attorneyName;
   public $hearingDate;
   public $alternateProgramStatus;
   public $complianceCompleted;
   public $appealType;
   public $appealStatus;
   public $reviewStatus;
   public $securityTransactionType;
   public $durationShortened;
   public $alternateResponseDuration;
   public $durationUnitOfMeasureCode;
   public $alternateDurationUnitOfMeasureCode;
   public $receivedServicesIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
