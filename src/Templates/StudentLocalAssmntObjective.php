<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentLocalAssmntObjective implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestSubjectArea;
   public $subtestGradeLevel;
   public $subtestVersionId;
   public $itemResponseIdentifier;
   public $itemResponseLevel;
   public $itemResponseGroup;
   public $itemResponseDetailDesc;
   public $maximumResponseValue;
   public $studentId;
   public $testDate;
   public $objectiveScoreType1;
   public $objectiveScore1;
   public $objectiveScoreValueType1;
   public $objectiveStandardAchieved1;
   public $studentMetObjectiveStandard1;
   public $objectiveScoreType2;
   public $objectiveScore2;
   public $objectiveScoreValueType2;
   public $objectiveStandardAchieved2;
   public $studentMetObjectiveStandard2;
   public $objectiveScoreType3;
   public $objectiveScore3;
   public $objectiveScoreValueType3;
   public $objectiveStandardAchieved3;
   public $studentMetObjectiveStandard3;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
