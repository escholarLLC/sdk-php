<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentLocalAssmntQuestion implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestSubjectArea;
   public $subtestGradeLevel;
   public $subtestVersionId;
   public $itemResponseIdentifier;
   public $itemResponseDetail;
   public $studentId;
   public $testDate;
   public $analyzedResultStatus;
   public $alphaValue;
   public $rawScore;
   public $responseStatus;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
