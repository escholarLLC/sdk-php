<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentLocalAssmntSubtest implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestSubjectArea;
   public $subtestGradeLevel;
   public $subtestVersionId;
   public $subtestCategory;
   public $subtestIdentifierOrganizationTypeCode;
   public $subtestMaximumScore;
   public $studentId;
   public $testDate;
   public $studentAssessmentStatus;
   public $studentAssessmentLanguageCode;
   public $studentAccommodationCode;
   public $scoreType1;
   public $score1;
   public $scoreValueType1;
   public $standardAchieved1;
   public $studentMetStandard1;
   public $scoreType2;
   public $score2;
   public $scoreValueType2;
   public $standardAchieved2;
   public $studentMetStandard2;
   public $scoreType3;
   public $score3;
   public $scoreValueType3;
   public $standardAchieved3;
   public $studentMetStandard3;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
