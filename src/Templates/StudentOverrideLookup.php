<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentOverrideLookup implements JsonSerializable {

   public $studentErrorDescription;
   public $overrideAllowed;
   public $testDescription;
   public $districtCode;
   public $schoolYearDate;
   public $studentLastName;
   public $studentFirstName;
   public $studentMiddleName;
   public $studentBirthDate;
   public $socialSecurityNumber;
   public $correctedStudentId;
   public $originalStudentId;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
