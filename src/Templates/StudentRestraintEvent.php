<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentRestraintEvent implements JsonSerializable {

   public $submittingDistrictCode;
   public $studentDistrictCode;
   public $studentId;
   public $schoolYearDate;
   public $restraintEventLocationCode;
   public $restraintEventId;
   public $restraintEventDate;
   public $reportingDate;
   public $restraintType;
   public $primaryDisabilityCode;
   public $studentInjuredIndicator;
   public $staffInjuredIndicator;
   public $restraintEventTimespanCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
