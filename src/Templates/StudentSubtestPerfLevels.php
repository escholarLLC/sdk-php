<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentSubtestPerfLevels implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $standardAchievedCode;
   public $scoreType;
   public $testDate;
   public $studentId;
   public $studentMetStandardIndicator;
   public $studentSchoolYearDate;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
