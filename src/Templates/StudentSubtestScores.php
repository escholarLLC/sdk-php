<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentSubtestScores implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $testDate;
   public $studentSchoolYearDate;
   public $studentId;
   public $scoreType;
   public $scoreValue;
   public $scoreValueType;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
