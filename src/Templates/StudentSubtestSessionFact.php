<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentSubtestSessionFact implements JsonSerializable {

   public $districtCode;
   public $testDescription;
   public $assessmentSchoolYearDate;
   public $subtestIdentifier;
   public $testDate;
   public $studentId;
   public $sessionName;
   public $studentSchoolYearDate;
   public $testBookletId;
   public $testBookletNumber;
   public $sessionStatusCode;
   public $sessionDate;
   public $sessionPlatformType;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
