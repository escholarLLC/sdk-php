<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class StudentSummaryAttendance implements JsonSerializable {

   public $districtCode;
   public $locationCode;
   public $schoolYearDate;
   public $studentId;
   public $attendancePeriodStartDate;
   public $attendancePeriodEndDate;
   public $daysPresent;
   public $daysAbsentTotal;
   public $daysEnrolled;
   public $daysAbsentWithoutExcuse;
   public $ftePercent;
   public $daysTardy;
   public $studentGradeLevel;
   public $maximumConsecutiveDaysAbsent;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
