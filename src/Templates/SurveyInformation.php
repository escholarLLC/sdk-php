<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SurveyInformation implements JsonSerializable {

   public $districtCode;
   public $surveyName;
   public $surveyAdministration;
   public $surveyOpenDate;
   public $surveyCloseDate;
   public $schoolYearDate;
   public $surveyShortDesc;
   public $surveyLongDesc;
   public $surveyScopeCode;
   public $surveyCategoryCode;
   public $totalPopulationSurveyed;
   public $suppressionThreshold;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
