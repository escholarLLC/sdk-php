<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SurveyParticipant implements JsonSerializable {

   public $districtCode;
   public $surveyName;
   public $surveyAdministration;
   public $participantId;
   public $participantTypeCode;
   public $participantLocationId;
   public $participantSurveyCompletionDate;
   public $raceOrEthnicityCode;
   public $genderCode;
   public $gradeLevel;
   public $homeLanguageCode;
   public $yearsOfExperience;
   public $studentId;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
