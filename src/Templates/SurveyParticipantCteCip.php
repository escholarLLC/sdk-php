<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SurveyParticipantCteCip implements JsonSerializable {

   public $districtCode;
   public $surveyName;
   public $surveyAdministration;
   public $surveyParticipationId;
   public $participantId;
   public $cipCode;
   public $cipSchoolYearDate;
   public $cipLocationCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
