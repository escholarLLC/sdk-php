<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SurveyParticipantResponse implements JsonSerializable {

   public $districtCode;
   public $surveyName;
   public $surveyAdministration;
   public $participantId;
   public $surveyQuestionId;
   public $surveyChoiceId;
   public $responseRank;
   public $freeFormResponseValue;
   public $surveyParticipationId;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
