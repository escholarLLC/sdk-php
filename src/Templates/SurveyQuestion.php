<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SurveyQuestion implements JsonSerializable {

   public $districtCode;
   public $surveyName;
   public $surveyAdministration;
   public $surveyQuestionId;
   public $surveyQuestionResponseTypeCode;
   public $surveyQuestionCategory1;
   public $surveyQuestionCategory2;
   public $surveyQuestionCategory3;
   public $surveyQuestionText;
   public $multipleResponseIndicator;
   public $maximumNumberOfResponsesAllowed;
   public $responseRequiresRankingIndicator;
   public $surveyQuestionSortSequence;
   public $surveyQuestionNumber;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
