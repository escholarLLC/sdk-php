<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class SurveyQuestionChoice implements JsonSerializable {

   public $districtCode;
   public $surveyName;
   public $surveyAdministration;
   public $surveyQuestionId;
   public $surveyChoiceId;
   public $surveyChoiceText;
   public $freeFormResponseAllowedIndicator;
   public $surveyChoiceIdRepresentsNoResponse;
   public $surveyChoiceIdRepresentsInvalidResponse;
   public $surveyChoiceSortSequence;
   public $freeFormNumericResponseIndicator;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
