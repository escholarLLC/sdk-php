<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class TenureArea implements JsonSerializable {

   public $districtCode;
   public $tenureAreaCode;
   public $schoolYearDate;
   public $tenureAreaShortDescription;
   public $tenureAreaLongDescription;
   public $tenureAreaCategory;
   public $tenureAreaStartDate;
   public $tenureAreaEndDate;
   public $tenureAreaSortSequence;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
