<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class TestDateLookup implements JsonSerializable {

   public $testDescription;
   public $testDate;
   public $schoolYearDate;
   public $testMonth;
   public $testYear;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
