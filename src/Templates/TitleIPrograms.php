<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class TitleIPrograms implements JsonSerializable {

   public $districtCode;
   public $schoolYearDate;
   public $studentId;
   public $reportingDate;
   public $titleIReadingEla;
   public $titleIMath;
   public $titleIScience;
   public $titleICivicsGovt;
   public $titleIHistory;
   public $titleIGeography;
   public $titleIInstructionalOther;
   public $titleIGuidanceCounseling;
   public $titleIHealth;
   public $titleINutrition;
   public $titleIPupilTransportation;
   public $titleIJobPreparation;
   public $titleIGed;
   public $titleIEnvironmentCode;
   public $titleIGeneralTutoring;
   public $titleIEarlyChildhoodEducation;
   public $titleIVocationalCareer;
   public $titleISupportOther;
   public $titleIInstructionalOtherDesc;
   public $titleISupportOtherDesc;
   public $studentSnapshotDate;
   public $titleISocialSciences;
   public $titleISocialWork;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
