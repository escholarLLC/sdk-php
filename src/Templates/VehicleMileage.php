<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class VehicleMileage implements JsonSerializable {

   public $districtCode;
   public $vehicleId;
   public $routeId;
   public $reportingDate;
   public $roadTypeCode;
   public $mileageTypeCode;
   public $dailyMileage;
   public $dailyDuration;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
