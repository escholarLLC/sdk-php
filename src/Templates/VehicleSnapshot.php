<?php namespace escholar\sdk\Templates;
use JsonSerializable;

class VehicleSnapshot implements JsonSerializable {

   public $districtCode;
   public $vehicleId;
   public $reportingDate;
   public $schoolYearDate;
   public $vehicleIdentificationNumber;
   public $licensePlateNumber;
   public $registrationStateCode;
   public $manufactureYear;
   public $bodyManufacturerCode;
   public $chassisManufacturerCode;
   public $grossVehicleWeightRating;
   public $fuelTypeCode;
   public $specialLiftEquipmentIndicator;
   public $seatingCapacity;
   public $registeredOwnerCode;
   public $vehicleContractorCode;
   public $lastInspectionDate;
   public $odometerMileage;
   public $odometerReadingDate;
   public $numberOfDaysInUse;
   public $vehicleInUseIndicator;
   public $districtOwnershipTypeCode;
   public $vehicleTypeCode;
   
   public function __construct($attributes = Array()) {
     foreach ($attributes as $field => $value) {
       $this->$field = $value;
     }
   }

   public function jsonSerialize() {
     $properties = get_object_vars($this);
     return $properties;
   }

}
?>
