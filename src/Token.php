<?php
namespace escholar\sdk;

use InvalidArgumentException;

class Token {
  private $loginId;
  private $wsKey;
  public $headerString;

  private function __construct($loginId, $wsKey) {
    $this->loginId = $loginId;
    $this->wsKey = $wsKey;

    $this->headerString = 'Bearer ' . $loginId . ':' . $wsKey;
  }

  public static function withCredentials($loginId, $wsKey) {
    if ($loginId === NULL || $wsKey === NULL) {
        throw new InvalidArgumentException('login and key are required');
    }

    return new Token($loginId, $wsKey);
  }
}

?>
