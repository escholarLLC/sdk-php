<?php
namespace escholar\Test\SDK;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use escholar\sdk\Token;

class TokenTest extends \PHPUnit_Framework_TestCase {
  private $user = 'SomeRandomUser';
  private $key = 'abc123xyz098';

  public function testCanCreateWithCredentials() {

    // make a new token with our test credentials
    $this->assertInstanceOf(
            Token::class,
            Token::withCredentials($this->user, $this->key)
        );
  }

  public function testTokenRequiresUser() {
    $this->expectException(InvalidArgumentException::class);

    // make a new token with our test credentials
    $this->assertInstanceOf(
            Token::class,
            Token::withCredentials(NULL, $this->key)
        );
  }

  public function testCreatesHeaderString() {
    $token = Token::withCredentials($this->user, $this->key);

    $this->assertEquals($token->headerString,
      "Bearer SomeRandomUser:abc123xyz098");
  }

}

?>
